/*populate roles */
insert into user_profile(type) values ('STAFF');  
insert into user_profile(type) values ('LEAD');
insert into user_profile(type) values ('PROJECT_ADMIN');
insert into user_profile(type) values ('SUPER_ADMIN');

INSERT INTO USERS (ID,FIRST_NAME,IS_ADMIN,LAST_NAME,LEAD_ID,PASSWORD,STATUS,USERNAME, ADDED_DATE, UPDATED_DATE) VALUES (1,'Super','Yes','Admin',1,'21232f297a57a5a743894a0e4a801fc3', 'Active', 'superadmin',SYSDATE,SYSDATE);

/* populate join table */
insert into user_user_profile (user_id, user_profile_id) select user.id, profile.id from users user, user_profile profile where user.username='admin' and profile.type='SUPER_ADMIN';
