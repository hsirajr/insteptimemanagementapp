package com.instep.tma.dao;

import java.util.List;

import com.instep.tma.model.User;

public interface UserDao {
	
	User findByLoginCredentials(String username, String password) throws Exception;
	
	void saveUser(User user);

	List<User> findAllUsers();

	User findById(Integer id);

	User findByUsername(String username);

	boolean savePasswordForNewUser(String username, String password) throws Exception;

	List<User> findAllAllocatedUsersByTaskId(int taskId);

	List<User> findAllNonAllocatedUsersByTaskId(int taskId);
	
	void deleteById(int id);

	List<User> findAllUsersByAdminRole();

}
