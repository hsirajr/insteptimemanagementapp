package com.instep.tma.dao;

import java.util.List;

import com.instep.tma.model.Client;

public interface ClientDao {
	
	List<Client> findAllClients();

	void saveClient(Client client);

	Client findById(int id);

	Client findByName(String name);

	void deleteById(int id);

}
