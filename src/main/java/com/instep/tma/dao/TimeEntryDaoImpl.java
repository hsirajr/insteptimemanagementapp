package com.instep.tma.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.instep.tma.model.UserSubmittedTimeLog;

@Repository("timeEntryDao")
public class TimeEntryDaoImpl extends AbstractDao<Integer, UserSubmittedTimeLog> implements TimeEntryDao {

	@Override
	public UserSubmittedTimeLog findById(int id) {
		UserSubmittedTimeLog userSubmittedTimeLog = getByKey(id);
		if (userSubmittedTimeLog != null) {
			Hibernate.initialize(userSubmittedTimeLog.getUser());
			Hibernate.initialize(userSubmittedTimeLog.getTask());
		}
		return userSubmittedTimeLog;
	}
	
	@Override
	public void save(UserSubmittedTimeLog userSubmittedTimeLog) {
		persist(userSubmittedTimeLog);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserSubmittedTimeLog> findAllUserSubmittedTimeLogsByUserIdAndLogDate(int userId, String logDate) {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("id"));
		criteria.add(Restrictions.eq("user.id", userId));
		criteria.add(Restrictions.eq("date", logDate));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<UserSubmittedTimeLog> userSubmittedTimeLogsList = (List<UserSubmittedTimeLog>) criteria.list();
		for (UserSubmittedTimeLog userSubmittedTimeLog : userSubmittedTimeLogsList) {
			if (userSubmittedTimeLog != null) {
				Hibernate.initialize(userSubmittedTimeLog.getUser());
				Hibernate.initialize(userSubmittedTimeLog.getTask());
			}
		}
		return userSubmittedTimeLogsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> findUserSubmittedTimeLogStatusByUserIdAndWeekIdentifierDate(int userId, String weekIndentifierDate) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user.id", userId));
		criteria.add(Restrictions.eq("date", weekIndentifierDate));
		criteria.setProjection(Projections.projectionList()
				.add(Projections.property("status")));
		List<String> statusForSelectedWeek = (List<String>) criteria.list();
		return statusForSelectedWeek;
	}

	@Override
	public UserSubmittedTimeLog findAllUserSubmittedTimeLogsByUserIdAndTaskIdAndLogDate(int userId, int taskId, String logDate) {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("id"));
		criteria.add(Restrictions.eq("user.id", userId));
		criteria.add(Restrictions.eq("task.id", taskId));
		criteria.add(Restrictions.eq("date", logDate));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		UserSubmittedTimeLog userSubmittedTimeLog = (UserSubmittedTimeLog) criteria.uniqueResult();
			if (userSubmittedTimeLog != null) {
				Hibernate.initialize(userSubmittedTimeLog.getUser());
				Hibernate.initialize(userSubmittedTimeLog.getTask());
		}
		return userSubmittedTimeLog;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserSubmittedTimeLog> findAllUserSubmittedTimeLogsByStatusesAsSubmittedAndResubmittedAndUserRole(String isUserAdmin) {
		Query query = getSession()
				.createSQLQuery("SELECT * FROM (SELECT dense_rank() over (partition BY l.log_date,l.user_id order by l.id DESC) AS rnk, l.* FROM user_submitted_time_log l LEFT OUTER JOIN users u ON (u.id=l.user_id) WHERE l.status='Submitted' OR l.status='Resubmitted' OR l.status='Approved' AND u.is_admin='No')WHERE rnk=1")
				.addEntity(UserSubmittedTimeLog.class);
		/*query.setParameter("timeSheetStatus", timesheetStatus);
		query.setParameter("isUserAdmin", isUserAdmin);
		*/List<UserSubmittedTimeLog> userSubmittedTimeLogsList = query.list();
		for (UserSubmittedTimeLog userSubmittedTimeLog : userSubmittedTimeLogsList) {
			if (userSubmittedTimeLog != null) {
				Hibernate.initialize(userSubmittedTimeLog.getUser());
				Hibernate.initialize(userSubmittedTimeLog.getTask());
			}
		}
		return userSubmittedTimeLogsList;
	}
	
}

