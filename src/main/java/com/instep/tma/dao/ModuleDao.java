package com.instep.tma.dao;

import java.util.List;

import com.instep.tma.model.Module;

public interface ModuleDao {
	
	List<Module> findAllModules();

	void saveModule(Module module);

	Module findById(Integer id);

	Module findByName(String name);

	List<Module> findAllModulesByProjectId(int projectId);

	void deleteById(int id);

}
