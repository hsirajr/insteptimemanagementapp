package com.instep.tma.dao;

import java.util.List;

import com.instep.tma.model.UserProfile;


public interface UserProfileDao {

	List<UserProfile> findAll();
	
	UserProfile findByType(String type);
	
	UserProfile findById(int id);
}
	