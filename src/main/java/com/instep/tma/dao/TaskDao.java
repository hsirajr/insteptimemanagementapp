package com.instep.tma.dao;

import java.util.List;

import com.instep.tma.model.Task;

public interface TaskDao {
	
	List<Task> findAllTasks();

	void saveTask(Task task);

	Task findById(Integer id);

	List<Task> findAllTasksByUser(int userId);

	Task findByName(String name);

	List<Task> findAllTasksByModuleId(int moduleId);

	void deleteById(int id);

}
