package com.instep.tma.dao;

import java.util.List;

import com.instep.tma.model.UserSubmittedTimeLog;

public interface TimeEntryDao {
	
	UserSubmittedTimeLog findById(int id);
	
	void save(UserSubmittedTimeLog userSubmittedTimeLog);

	List<UserSubmittedTimeLog> findAllUserSubmittedTimeLogsByUserIdAndLogDate(int userId, String logDate);

	List<String> findUserSubmittedTimeLogStatusByUserIdAndWeekIdentifierDate(int userId, String weekIdentifierDate);

	UserSubmittedTimeLog findAllUserSubmittedTimeLogsByUserIdAndTaskIdAndLogDate(int userId, int taskId, String logDate);

	List<UserSubmittedTimeLog> findAllUserSubmittedTimeLogsByStatusesAsSubmittedAndResubmittedAndUserRole(String isUserAdmin);
	
}
