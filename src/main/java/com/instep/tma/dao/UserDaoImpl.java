package com.instep.tma.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import com.instep.tma.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	@Autowired
	private PasswordEncoder passwordEncoder;	

	@Override
	public User findByLoginCredentials(String username, String password) throws Exception {
		try {
			String hashedPassword = passwordEncoder.encode(password);
			Criteria crit = createEntityCriteria();
			crit.add(Restrictions.and(
					Restrictions.eq("username", username),
					Restrictions.eq("password", hashedPassword)));
			User user = (User) crit.uniqueResult();

			if (user != null) {
				Hibernate.initialize(user.getUserProfiles());Hibernate.initialize(user.getTasks());
			}

			return user;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	@Override
	public void saveUser(User user) {
		persist(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUsers() {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("firstName"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);// To avoid
		List<User> users = (List<User>) criteria.list();
		for (User user : users) {
			if (user != null) {
				Hibernate.initialize(user.getUserProfiles());Hibernate.initialize(user.getTasks());
			}
		}	
		return users;

	}

	@Override
	public User findById(Integer id) {
		User user = getByKey(id);
		if (user != null) {
			Hibernate.initialize(user.getUserProfiles());Hibernate.initialize(user.getTasks());
		}
		return user;
	}

	@Override
	public User findByUsername(String username) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("username", username));
		User user = (User) crit.uniqueResult();
		if (user != null) {
			Hibernate.initialize(user.getUserProfiles());Hibernate.initialize(user.getTasks());
		}
		return user;
	}

	@Override
	public boolean savePasswordForNewUser(String username, String password) throws Exception {
		User user = findByUsername(username);
		if(user == null)
			return false;
		String hashedPassword = passwordEncoder.encode(password);
		user.setPassword(hashedPassword);
		saveUser(user);
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllAllocatedUsersByTaskId(int taskId) {
		Query query = getSession()
				.createSQLQuery(
						"select u.* from users u, user_task ut, tasks t where t.id = ut.task_id and u.id = ut.user_id and t.id = :taskId")
				.addEntity(User.class);
		query.setParameter("taskId", taskId);
		List<User> users = (List<User>) query.list();
		for (User user : users) {
			if (user != null) {
				Hibernate.initialize(user.getUserProfiles());Hibernate.initialize(user.getTasks());
			}
		}	
		return users;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllNonAllocatedUsersByTaskId(int taskId) {
		
		Query ifTaskExistsInTheJoinTableQuery = getSession()
				.createSQLQuery(
						"select u.* from tasks t, user_task ut, users u where t.id = ut.task_id and u.id = ut.user_id and t.id = :taskId")
				.addEntity(User.class);
		ifTaskExistsInTheJoinTableQuery.setParameter("taskId", taskId);
		if(ifTaskExistsInTheJoinTableQuery.list().isEmpty()) { //if task does not preexists in the user_task join table return all users
			return findAllUsers();
		}
		
		Query query = getSession()
				/*.createSQLQuery("select distinct u.* from users u, user_task ut, tasks t where t.id = ut.task_id and u.id = ut.user_id and t.id != :taskId and ut.user_id not in (select user_id from user_task where task_id = :taskId)")
				.addEntity(User.class);*/
				.createSQLQuery("select distinct u.* from users u, user_task ut, tasks t where t.id != :taskId and u.id not in (select user_id from user_task where task_id = :taskId)")
				.addEntity(User.class);
		
		query.setParameter("taskId", taskId);
		List<User> users = (List<User>) query.list();
		for (User user : users) {
			if (user != null) {
				Hibernate.initialize(user.getUserProfiles());Hibernate.initialize(user.getTasks());
			}
		}	
		return users;
	}
	
	public void deleteById(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		User user = (User) crit.uniqueResult();
		delete(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUsersByAdminRole() {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("firstName"));
		criteria.add(Restrictions.eq("isAdmin", "Yes"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<User> usersListWithAdminRole = criteria.list();
		return usersListWithAdminRole;
	}
}
