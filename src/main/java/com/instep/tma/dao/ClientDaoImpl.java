package com.instep.tma.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.instep.tma.model.Client;
import com.instep.tma.model.User;
import com.instep.tma.dao.AbstractDao;

@Repository("clientDao")
public class ClientDaoImpl extends AbstractDao<Integer, Client> implements ClientDao {

	@Override
	public Client findById(int id) {
		Client client = getByKey(id);
		if(client != null) {
			Hibernate.initialize(client.getProjects());
		}
		return client;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Client> findAllClients() {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("name"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);// To avoid
		List<Client> clients = (List<Client>) criteria.list();
		for (Client client : clients) {
			if(client != null) {
				Hibernate.initialize(client.getProjects());
			}
		}
		return clients;
	}

	@Override
	public void saveClient(Client client) {
		persist(client);
	}

	@Override
	public Client findByName(String name) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("name", name));
		Client client = (Client) crit.uniqueResult();
		if(client != null) {
			Hibernate.initialize(client.getProjects());
		}
		return client;
	}
	
	public void deleteById(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Client client = (Client) crit.uniqueResult();
		delete(client);
	}


}

