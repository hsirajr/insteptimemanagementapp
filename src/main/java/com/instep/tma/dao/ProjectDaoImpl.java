package com.instep.tma.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.instep.tma.model.Project;
import com.instep.tma.dao.AbstractDao;

@Repository("projectDao")
public class ProjectDaoImpl extends AbstractDao<Integer, Project> implements ProjectDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> findAllProjects() {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("name"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);// To avoid
		List<Project> projects = (List<Project>) criteria.list();
		for (Project project : projects) {
			if (project != null) {
				Hibernate.initialize(project.getClient());
			//	Hibernate.initialize(project.getModules());
			}
		}
		return projects;
	}

	@Override
	public void saveProject(Project project) {
		persist(project);
	}

	@Override
	public Project findById(Integer id) {
		Project project = getByKey(id);
		if (project != null) {
			//Hibernate.initialize(project.getModules());
			Hibernate.initialize(project.getClient());
		}
		return project;
	}

	@Override
	public Project findByName(String name) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("name", name));
		Project project = (Project) crit.uniqueResult();
		if(project != null) {
			//Hibernate.initialize(project.getModules());
			Hibernate.initialize(project.getClient());
		}
		return project;
	}

	@Override
	public void deleteById(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Project project = (Project) crit.uniqueResult();
		delete(project);
	}
	
}

