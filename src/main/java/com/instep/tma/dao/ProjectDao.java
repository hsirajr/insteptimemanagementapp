package com.instep.tma.dao;

import java.util.List;

import com.instep.tma.model.Project;

public interface ProjectDao {
	
	List<Project> findAllProjects();

	void saveProject(Project project);

	Project findById(Integer id);

	Project findByName(String name);

	void deleteById(int id);

}
