package com.instep.tma.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.instep.tma.model.Task;
import com.instep.tma.model.User;
import com.instep.tma.dao.AbstractDao;

@Repository("taskDao")
public class TaskDaoImpl extends AbstractDao<Integer, Task> implements TaskDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> findAllTasks() {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("name"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);// To avoid
		List<Task> tasks = (List<Task>) criteria.list();
		for (Task task : tasks) {
			if (task != null) {
				Hibernate.initialize(task.getModule());
				Hibernate.initialize(task.getModule().getProject());
				Hibernate.initialize(task.getUsers());
				Hibernate.initialize(task.getUserSubmittedTimeLogs());
			}
		}
		return tasks;
	}

	@Override
	public void saveTask(Task task) {
		persist(task);
	}

	@Override
	public Task findById(Integer id) {
		Task task = getByKey(id);
		if (task != null) {
			Hibernate.initialize(task.getUserSubmittedTimeLogs());
			Hibernate.initialize(task.getUsers());
			Hibernate.initialize(task.getModule());
			Hibernate.initialize(task.getModule().getProject());
		}
		return task;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> findAllTasksByUser(int userId) {
		Query query = getSession()
				.createSQLQuery(
						"select t.* from tasks t, user_task ut, users u where t.id = ut.task_id and u.id = ut.user_id and u.id = :userId")
				.addEntity(Task.class);
		query.setParameter("userId", userId);
		List<Task> tasks = (List<Task>) query.list();
		for (Task task : tasks) {
			if (task != null) {
				Hibernate.initialize(task.getModule());
				Hibernate.initialize(task.getModule().getProject());
				Hibernate.initialize(task.getUsers());
				Hibernate.initialize(task.getUserSubmittedTimeLogs());
			}
		}
		return tasks;
	}

	@Override
	public Task findByName(String name) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("name", name));
		Task task = (Task) crit.uniqueResult();
		if(task != null) {
			Hibernate.initialize(task.getModule());
			Hibernate.initialize(task.getModule().getProject());
			Hibernate.initialize(task.getUsers());
			Hibernate.initialize(task.getUserSubmittedTimeLogs());
		}
		return task;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Task> findAllTasksByModuleId(int moduleId) {
		Criteria criteria = createEntityCriteria().addOrder(Order.asc("name"));
		criteria.add(Restrictions.eq("module.id", moduleId));
		criteria.add(Restrictions.eq("status", "Active"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<Task> tasksListByProjectId = criteria.list();
		return tasksListByProjectId;
	}
	
	public void deleteById(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Task task = (Task) crit.uniqueResult();
		delete(task);
	}
}

