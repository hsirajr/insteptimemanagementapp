package com.instep.tma.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.instep.tma.model.Module;
import com.instep.tma.dao.AbstractDao;

@Repository("moduleDao")
public class ModuleDaoImpl extends AbstractDao<Integer, Module> implements ModuleDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Module> findAllModules() {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("name"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);// To avoid
		List<Module> modules = (List<Module>) criteria.list();
		for (Module module : modules) {
			if (module != null) {
				Hibernate.initialize(module.getProject());
				Hibernate.initialize(module.getTasks());
			}
		}
		return modules;
	}

	@Override
	public void saveModule(Module module) {
		persist(module);
	}

	@Override
	public Module findById(Integer id) {
		Module module = getByKey(id);
		if (module != null) {
			Hibernate.initialize(module.getTasks());
			Hibernate.initialize(module.getProject());
		}
		return module;
	}

	@Override
	public Module findByName(String name) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("name", name));
		Module module = (Module) crit.uniqueResult();
		if(module != null) {
			Hibernate.initialize(module.getTasks());
			Hibernate.initialize(module.getProject());
		}
		return module;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Module> findAllModulesByProjectId(int projectId) {
		Criteria criteria = createEntityCriteria().addOrder(
				Order.asc("name"));
		criteria.add(Restrictions.eq("project.id", projectId));
		criteria.add(Restrictions.eq("status", "Active"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);// To avoid
		List<Module> modulesByProjectId = (List<Module>) criteria.list();
		for (Module module : modulesByProjectId) {
			if (module != null) {
				Hibernate.initialize(module.getProject());
				Hibernate.initialize(module.getProject().getClient());
				Hibernate.initialize(module.getTasks());
			}
		}
		return modulesByProjectId;
	}

	@Override
	public void deleteById(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Module module = (Module) crit.uniqueResult();
		delete(module);
	}
}

