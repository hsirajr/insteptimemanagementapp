package com.instep.tma.dto;

import java.util.List;

import javax.validation.Valid;

import com.instep.tma.model.UserSubmittedTimeLog;

public class UserSubmittedLogsListDTO {

	@Valid
	private List<UserSubmittedTimeLog> userSubmittedTimeLogsList;
	
	private String totalPtoHoursPerWeek;

	public List<UserSubmittedTimeLog> getUserSubmittedTimeLogsList() {
		return userSubmittedTimeLogsList;
	}

	public void setUserSubmittedTimeLogsList(List<UserSubmittedTimeLog> userSubmittedTimeLogsList) {
		this.userSubmittedTimeLogsList = userSubmittedTimeLogsList;
	}
	
	public String getTotalPtoHoursPerWeek() {
		return totalPtoHoursPerWeek;
	}

	public void setTotalPtoHoursPerWeek(String totalPtoHoursPerWeek) {
		this.totalPtoHoursPerWeek = totalPtoHoursPerWeek;
	}

	@Override
	public String toString() {
		return "UserSubmittedLogsListDTO [userSubmittedTimeLogsList=" + userSubmittedTimeLogsList + "]";
	}    


}
