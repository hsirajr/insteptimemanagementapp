package com.instep.tma.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.collections4.map.MultiValueMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.instep.tma.dto.UserSubmittedLogsListDTO;
import com.instep.tma.model.Task;
import com.instep.tma.model.User;
import com.instep.tma.model.UserSubmittedTimeLog;
import com.instep.tma.service.EmailService;
import com.instep.tma.service.TaskService;
import com.instep.tma.service.TimeEntryService;
import com.instep.tma.service.UserService;

@Controller
@PropertySource(value = { "classpath:application.properties" })
public class TimeManagementController {
	
	@Autowired
	TimeEntryService timeEntryService;
	@Autowired
	TaskService taskService;
	@Autowired
	UserService userService;
	@Autowired
	EmailService emailService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/view-timesheets" }, method = RequestMethod.GET)
	public String viewTimesheetsPage(ModelMap model, HttpSession session , @ModelAttribute("message") final  String message) throws Exception {
		int loggedInUser = (Integer) session.getAttribute("loggedInUser"); String loggedInUserName = ((User) session.getAttribute("loggedInUserObject")).getFirstName() + " " + ((User) session.getAttribute("loggedInUserObject")).getLastName();model.addAttribute("loggedInUserName",loggedInUserName);
		Map<String, String> previousFiveWeeksStatusMap = timeEntryService.getPreviousFiveWeeksStatusByUserId(loggedInUser);
		Map<String, String> currentWeekAndUpcomingFourWeeksStatusMap = timeEntryService.getCurrentWeekAndUpcomingFourWeeksStatusByUserId(loggedInUser);
		User user = userService.findById(loggedInUser);
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("previousFiveWeeksStatusMap", previousFiveWeeksStatusMap);
		model.addAttribute("currentWeekAndUpcomingFourWeeksStatusMap", currentWeekAndUpcomingFourWeeksStatusMap);
		model.addAttribute("previousFiveWeeksMap", timeEntryService.getPreviousFiveWeeks());
		model.addAttribute("currentWeekAndUpcomingFourWeeksMap", timeEntryService.getCurrentWeekAndUpcomingFourWeeks());
		model.addAttribute("message",message);
		return "view-timesheets";
	}
	
	@RequestMapping(value = { "/time-entry-{selectedWeekDatesList}" }, method = RequestMethod.GET) //change to @valid, remove @pathvariable
	public String viewTimeEntryPage(@PathVariable List<String> selectedWeekDatesList, ModelMap model, HttpSession session) throws Exception {
		int loggedInUser = (Integer) session.getAttribute("loggedInUser"); String loggedInUserName = ((User) session.getAttribute("loggedInUserObject")).getFirstName() + " " + ((User) session.getAttribute("loggedInUserObject")).getLastName();model.addAttribute("loggedInUserName",loggedInUserName);
		UserSubmittedLogsListDTO userSubmittedTimeLogsListDTO = new UserSubmittedLogsListDTO();
		List<UserSubmittedTimeLog> userSubmittedTimeLogsList = timeEntryService.findAllUserSubmittedTimeLogsByUserIdAndLogDate(loggedInUser, selectedWeekDatesList.get(6));
		if(userSubmittedTimeLogsList != null) {
			userSubmittedTimeLogsListDTO.setUserSubmittedTimeLogsList(userSubmittedTimeLogsList);
		}
		MultiValueMap<Integer, Task> tasksForCurrentUserPerModuleMap = taskService.findAllTasksByUser(loggedInUser);
		User user = userService.findById(loggedInUser);
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("loggedInUser", loggedInUser);
		model.addAttribute("selectedWeekDatesList", selectedWeekDatesList);
		model.addAttribute("tasksForCurrentUserPerModuleMap", tasksForCurrentUserPerModuleMap);
		model.addAttribute("userSubmittedTimeLogsList", userSubmittedTimeLogsListDTO);
		model.addAttribute("totalPtoHoursPerWeek",userSubmittedTimeLogsList.size() > 0 ?  userSubmittedTimeLogsList.get(0).getTotalPtoHoursPerWeek() : "0.00,0.00,0.00,0.00,0.00,0.00,0.00");
		model.addAttribute("userName" , user.getFirstName() + " " + user.getLastName());
		
		return "time-entry";
	}
	
	@RequestMapping(value = { "/time-entry" }, method = RequestMethod.POST)
	public String submitTimeEntry(@Valid UserSubmittedLogsListDTO userSubmittedTimeLogsListDTO, BindingResult result, ModelMap model, HttpSession session,final RedirectAttributes redirectAttrs) throws Exception {
		if(result.hasErrors()) {
			model.addAttribute("error", "An error occurred while updating the timesheet.");
			return "time-entry";
		}
		
		boolean isUserSubmittedLogSavedOrUpdatedSuccessfully = timeEntryService.saveUserSubmittedLog(userSubmittedTimeLogsListDTO);
		int loggedInUser = (Integer) session.getAttribute("loggedInUser"); String loggedInUserName = ((User) session.getAttribute("loggedInUserObject")).getFirstName() + " " + ((User) session.getAttribute("loggedInUserObject")).getLastName();model.addAttribute("loggedInUserName",loggedInUserName);
		User user = userService.findById(loggedInUser);
		User lead = userService.findById(user.getLeadId()); 
		Map<String, String> emailContentMap = new HashMap<>();
		if(isUserSubmittedLogSavedOrUpdatedSuccessfully) {
			if(lead != null){
				emailContentMap.put("emailTo", lead.getUsername());
				emailContentMap.put("subject", "INSTEP Time Management - Request for Time-sheet Review");
				emailContentMap.put("text", "Dear " + lead.getFirstName() + " " + lead.getLastName() + "\n" + user.getFirstName() + " " + user.getLastName() + " has requested you to review their timesheet for the week ending " + userSubmittedTimeLogsListDTO.getUserSubmittedTimeLogsList().get(0).getDate()  + ".\nRegards, \nINSTEP Time App Admin");
				emailService.sendNotificationViaEmail(emailContentMap);
			}
			model.addAttribute("success", "Timesheet updated successfully.");
		}
		else
			model.addAttribute("error", "An error occurred while updating the timesheet.");
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("post", true);
		model.addAttribute("userName" , user.getFirstName() + " " + user.getLastName());
		redirectAttrs.addFlashAttribute("message", "Timesheet updated successfully.");
		if(user.getIsAdmin().equals("Yes")){
			return "redirect:/team-timesheets";
		} else{
			return "redirect:/view-timesheets";
		}
	}
	
}
