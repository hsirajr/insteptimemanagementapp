package com.instep.tma.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.instep.tma.model.Module;
import com.instep.tma.model.Task;
import com.instep.tma.model.User;
import com.instep.tma.service.EmailService;
import com.instep.tma.service.ModuleService;
import com.instep.tma.service.TaskService;
import com.instep.tma.service.UserService;
import com.instep.tma.util.AppConstants;

@RestController
@PropertySource(value = { "classpath:application.properties" })
public class AdminRestController {

	@Autowired
	ModuleService moduleService;
	
	@Autowired
	TaskService taskService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	EmailService emailService;
	
	@RequestMapping(value = "/getModulesForSelectedProject", method = RequestMethod.GET)
	public ResponseEntity<?> getModulesForSelectedProject(@RequestParam int projectId) throws Exception {
		System.out.println("-----------=projectId " + projectId);
		List<Module> modulesListByProjectId = moduleService.findAllModulesByProjectId(projectId);
		//System.out.println("-----------=modulesListByProjectId " + modulesListByProjectId.get(0).getProject().getId());
		if (modulesListByProjectId.isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Module>>(modulesListByProjectId, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getTasksForSelectedModule", method = RequestMethod.GET)
	public ResponseEntity<?> getTasksForSelectedModule(@RequestParam int moduleId) throws Exception {
		System.out.println("-----------=moduleId " + moduleId);
		List<Task> tasksListByModuleId = taskService.findAllTasksByModuleId(moduleId);
		//System.out.println("-----------=tasksListByModuleId " + tasksListByModuleId.get(0).getModule().getId());
		if (tasksListByModuleId.isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<Task>>(tasksListByModuleId, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getAllocatedUsersForSelectedTask", method = RequestMethod.GET)
	public ResponseEntity<?> getAllocatedUsersForSelectedtask(@RequestParam int taskId) throws Exception {
		System.out.println("-----------=taskId " + taskId);
		List<User> allocatedUsersListByTaskId = userService.findAllAllocatedUsersByTaskId(taskId);
		//System.out.println("-----------=tasksListByModuleId " + tasksListByModuleId.get(0).getModule().getId());
		if (allocatedUsersListByTaskId.isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<User>>(allocatedUsersListByTaskId, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getNonAllocatedUsersForSelectedTask", method = RequestMethod.GET)
	public ResponseEntity<?> getNonAllocatedUsersForSelectedtask(@RequestParam int taskId) throws Exception {
		System.out.println("-----------=taskId " + taskId);
		List<User> nonAllocatedUsersListByTaskId = userService.findAllNonAllocatedUsersByTaskId(taskId);
		//System.out.println("-----------=tasksListByModuleId " + tasksListByModuleId.get(0).getModule().getId());
		if (nonAllocatedUsersListByTaskId.isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<List<User>>(nonAllocatedUsersListByTaskId, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	public ResponseEntity<?> resetPassword(@RequestBody Map<String, String> parameters) throws Exception {
		System.out.println("-----------=userId " + parameters.get("userId"));
		Map<String, String> emailContentMap = new HashMap<>();
		User user = userService.resetPassword(Integer.parseInt(parameters.get("userId")));
		if (user != null) {
			emailContentMap.put("emailTo", user.getUsername());
			emailContentMap.put("subject", "INSTEP Time App - Password Reset - Notification");
			emailContentMap.put("text", "Dear " + user.getFirstName() + ",\n\nAdministrator has sent request to reset your password.You can reset your password from following URL :  \n" + AppConstants.SERVER_URL + "sign-up-" + user.getUsername() + " \n\nRegards, \nINSTEP Time App Admin");
			emailService.sendNotificationViaEmail(emailContentMap);
			return new ResponseEntity<String>(HttpStatus.OK);
		} else {
			return new ResponseEntity<List<User>>(HttpStatus.NOT_FOUND);
		}
	}
	
}
