package com.instep.tma.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.instep.tma.model.User;
import com.instep.tma.service.UserService;

@Controller
@PropertySource(value = { "classpath:application.properties" })
public class UserController {

	@Autowired
	UserService userService;
	
	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;

	/*@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String redirectToAppInitialView(HttpSession session) throws Exception {
		if(session.getAttribute("loggedInUser") == null) {
			System.out.println("redirecting to login page ");
			return "redirect:/login";
		} else {
			System.out.println("redirecting to home page ");
			return "redirect:/home";
		}
	}*/

/*	@SuppressWarnings("unused")
	@RequestMapping(value = { "/login" }, method = RequestMethod.POST)
	public ResponseEntity<?> redirectFromLoginPage(@RequestBody Map<String, String> loginCredentialsMap, UriComponentsBuilder uriComponentsBuilder, HttpSession session) throws Exception {
		String username = loginCredentialsMap.get("username");
		String password = loginCredentialsMap.get("password");
		User user = userService.findByLoginCredentials(username, password);

		HttpHeaders headers = new HttpHeaders();
		if(user != null) {
			session.setAttribute("loggedInUser", user.getId());
			session.setAttribute("loggedInUserObject", user);
			UriComponents uriComponents = uriComponentsBuilder.path("/home").build();
			headers.setLocation(uriComponents.toUri());
			return new ResponseEntity<Void>(headers, HttpStatus.OK);
		} else{
			session.setAttribute("loggedInUser", null);
			session.setAttribute("loggedInUserObject", user);
			UriComponents uriComponents = uriComponentsBuilder.path("/login").queryParam("error", "").build();
			headers.setLocation(uriComponents.toUri());
			return new ResponseEntity<Void>(headers, HttpStatus.OK);
		}
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String viewLoginPage() throws Exception {
		return "login";
	}*/

	@RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	public String viewHomePage(ModelMap model, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal());
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName());
		model.addAttribute("loggedInUserObject", user);
		return "home";
	}

	/*@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public String logout(HttpSession session) {
		session.removeAttribute("loggedInUser");
		return "redirect:/login?logout";
	}*/

	@RequestMapping(value = { "/sign-up-{username}" }, method = RequestMethod.GET)
	public String viewSignUpPage(@PathVariable String username, ModelMap model, HttpSession session) throws Exception {
		model.addAttribute("username",username);
		return "sign-up";
	}
	
	/*@SuppressWarnings("unused")
	@RequestMapping(value = { "/sign-up" }, method = RequestMethod.POST)
	public ResponseEntity<?> registerPasswordForNewUser(@RequestBody Map<String, String> loginCredentialsMap, UriComponentsBuilder uriComponentsBuilder, HttpSession session) throws Exception {
		String username = loginCredentialsMap.get("username");
		String password = loginCredentialsMap.get("password");
		HttpHeaders headers = new HttpHeaders();
		boolean isUsernameRegistered = userService.savePasswordForNewUser(username, password);
		if(isUsernameRegistered) {
			System.out.println("sign up success");
			return "redirect:/sign-up?success";
		} else {
			UriComponents uriComponents = uriComponentsBuilder.path("/sign-up?error").build();
			headers.setLocation(uriComponents.toUri());
			System.out.println("sign up error");
			return new ResponseEntity<Void>(headers, HttpStatus.NOT_FOUND);
		}
	}*/

	@SuppressWarnings("unused")
	@RequestMapping(value = { "/sign-up" }, method = RequestMethod.POST)
	public ResponseEntity<?> registerPasswordForNewUser(@RequestBody Map<String, String> loginCredentialsMap, UriComponentsBuilder uriComponentsBuilder, HttpSession session) throws Exception {
		String username = loginCredentialsMap.get("username");
		String password = loginCredentialsMap.get("password");
		HttpHeaders headers = new HttpHeaders();
		boolean isUsernameRegistered = userService.savePasswordForNewUser(username, password);
		if(isUsernameRegistered) {
			return new ResponseEntity<String>(HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	
	
	/**
	 * This method handles login GET requests. If users is already logged-in and
	 * tries to goto login page again, will be redirected to userslist page.
	 */
	@RequestMapping(value = { "/" , "/login" }, method = RequestMethod.GET)
	public String loginPage() {
		if (isCurrentAuthenticationAnonymous()) {
			return "login";
		} else {
			return "redirect:/home";
		}
	}

	/**
	 * This method handles logout requests. Toggle the handlers if you are
	 * RememberMe functionality is useless in your app.
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request,
			HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null) {
			// new SecurityContextLogoutHandler().logout(request, response,
			// auth);
			persistentTokenBasedRememberMeServices.logout(request, response,
					auth);
			SecurityContextHolder.getContext().setAuthentication(null);
		}
		return "redirect:/login?logout";
	}
	
	/**
	 * This method handles Access-Denied redirect.
	 */
	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("loggedinuser", userService.findByUsername(getPrincipal()));
		return "accessDenied";
	}
	
	/**
	 * This method returns the principal[user-name] of logged-in user.
	 */
	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}

	/**
	 * This method returns true if users is already authenticated [logged-in],
	 * else false.
	 */
	private boolean isCurrentAuthenticationAnonymous() {
		final Authentication authentication = SecurityContextHolder
				.getContext().getAuthentication();
		return authenticationTrustResolver.isAnonymous(authentication);
	}
	
}
