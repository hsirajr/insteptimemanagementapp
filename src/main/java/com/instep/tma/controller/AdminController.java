package com.instep.tma.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.collections4.map.MultiValueMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.instep.tma.dto.UserSubmittedLogsListDTO;
import com.instep.tma.model.Client;
import com.instep.tma.model.Module;
import com.instep.tma.model.Project;
import com.instep.tma.model.Task;
import com.instep.tma.model.User;
import com.instep.tma.model.UserProfile;
import com.instep.tma.model.UserSubmittedTimeLog;
import com.instep.tma.service.ClientService;
import com.instep.tma.service.EmailService;
import com.instep.tma.service.ModuleService;
import com.instep.tma.service.ProjectService;
import com.instep.tma.service.TaskService;
import com.instep.tma.service.TimeEntryService;
import com.instep.tma.service.UserProfileService;
import com.instep.tma.service.UserService;
import com.instep.tma.util.AppConstants;

@Controller
@PropertySource(value = { "classpath:application.properties" })
@SessionAttributes({"roles"})
public class AdminController {

	@Autowired
	UserService userService;
	
	@Autowired
	ClientService clientService;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	ModuleService moduleService;
	
	@Autowired
	TaskService taskService;
	
	@Autowired
	TimeEntryService timeEntryService;
	
	@Autowired
	EmailService emailService;

	@Autowired
	UserProfileService userProfileService;
	
	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
	
	@SuppressWarnings("unused")
	@RequestMapping(value = { "/add-user" }, method = RequestMethod.GET)
	public String viewAddUserPage(ModelMap model, HttpSession session) throws Exception {
		User user = new User();
		List<User> usersListWithAdminRole = userService.findAllUsersByAdminRole();
		User userByLoggedInUserId = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", userByLoggedInUserId.getFirstName() + " " + userByLoggedInUserId.getLastName()); model.addAttribute("loggedInUserObject", userByLoggedInUserId);
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("user", user);
		model.addAttribute("usersListWithAdminRole", usersListWithAdminRole);
		return "add-user";
	}

	@RequestMapping(value = { "/add-user" }, method = RequestMethod.POST)
	public String addUser(@Valid User user, BindingResult result, ModelMap model, HttpSession session) throws Exception {
		System.out.println("saving user: " + user.toString());
		Map<String, String> emailContentMap = new HashMap<>();
		User newUser = new User();
		List<User> usersListWithAdminRole = userService.findAllUsersByAdminRole();
		User userByLoggedInUserId = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		model.addAttribute("usersListWithAdminRole", usersListWithAdminRole);
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		if(result.hasErrors()) {
			model.addAttribute("error", "An error occurred while creating the user.");
			return "add-user";
		}
		
		user.setUsername(user.getUsername() + "@instepinc.com");
		
		User userByUsername = userService.findByUsername(user.getUsername());
		if(userByUsername != null) {
			model.addAttribute("error", "A user with the provided username already exists. Please try something else.");
			return "add-user";
		}
		
		userService.saveUser(user);
		
		emailContentMap.put("emailTo", user.getUsername());
		emailContentMap.put("subject", "INSTEP Time App- Account created -Request for Password Creation");
		emailContentMap.put("text", "Dear " + user.getFirstName() + ",\n\nAdmin has created your account using your INSTEP email address. Please create your password from following URL : \n" + AppConstants.SERVER_URL + "sign-up-" + user.getUsername() + " \n\nRegards, \nINSTEP Time App Admin");
		emailService.sendNotificationViaEmail(emailContentMap);
		
		model.addAttribute("success", "User successfully created.");
		model.addAttribute("user", newUser);
		return "add-user";
	}
	
	@RequestMapping(value = { "/view-users" }, method = RequestMethod.GET)
	public String viewAllUsersPage(ModelMap model, HttpSession session) throws Exception {
		List<User> users = userService.findAllUsers();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("users", users);
		return "view-users";
	}
	
	@RequestMapping(value = { "/edit-user-{id}" }, method = RequestMethod.GET)
	public String editUserPage(@PathVariable int id, ModelMap model, HttpSession session) throws Exception {
		User user = userService.findById(id);
		List<User> usersListWithAdminRole = userService.findAllUsersByAdminRole();
		 model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("usersListWithAdminRole", usersListWithAdminRole);
		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		return "add-user";
	}
	
	@RequestMapping(value = { "/edit-user-{id}" }, method = RequestMethod.POST)
	public String updateUser(@Valid User user, BindingResult result,
			ModelMap model, @PathVariable String id, HttpServletRequest request, HttpSession session) throws Exception {

		userService.updateUser(user);
		List<User> usersListWithAdminRole = userService.findAllUsersByAdminRole();
		model.addAttribute("usersListWithAdminRole", usersListWithAdminRole);
		model.addAttribute("users", userService.findAllUsers());
		return "redirect:/view-users";
	}
	
	@RequestMapping(value = { "/inactivate-user-{id}" }, method = RequestMethod.GET)
	public String inactivateUser(@PathVariable int id, HttpServletRequest request, ModelMap model, HttpSession session) {
		System.out.println("deleting user with id: " + id);
		userService.inactivateUserById(id);
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		List<User> users = userService.findAllUsers();
		/*if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);*/
		
		model.addAttribute("users", users);
		return "redirect:/view-users";
	}

	@RequestMapping(value = { "/add-project" }, method = RequestMethod.GET)
	public String viewAddProjectPage(ModelMap model, HttpSession session) throws Exception {
		Project project = new Project();
		List<Client> clientsList = clientService.findAllClients();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("project", project);
		model.addAttribute("clientsList", clientsList);
		return "add-project";
	}

	@RequestMapping(value = { "/add-project" }, method = RequestMethod.POST)
	public String addProject(@Valid Project project, BindingResult result, ModelMap model, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		Project newProject = new Project();
		List<Client> clientsList = clientService.findAllClients();
		
		
		model.addAttribute("clientsList", clientsList);
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);

		if(result.hasErrors()) {
			model.addAttribute("error", "An error occurred while creating the project.");
			return "add-project";
		}
		
		Project projectByProjectName = projectService.findByName(project.getName());
		if(projectByProjectName != null) {
			model.addAttribute("error", "A project with the same name already exists.");
			return "add-project";
		}
		
		
		projectService.saveProject(project);
		model.addAttribute("success", "Project successfully created.");
		model.addAttribute("project", newProject);
		return "add-project";
	}
	

	@RequestMapping(value = { "/view-projects" }, method = RequestMethod.GET)
	public String viewAllProjectsPage(ModelMap model, HttpSession session) throws Exception {
		List<Project> projects = projectService.findAllProjects();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("projects", projects);
		return "view-projects";
	}
	
	@RequestMapping(value = { "/edit-project-{id}" }, method = RequestMethod.GET)
	public String editProjectPage(@PathVariable int id, ModelMap model, HttpSession session) throws Exception {
		Project project = projectService.findById(id);
		List<Client> clientsList = clientService.findAllClients();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("project", project);
		model.addAttribute("clientsList", clientsList);
		model.addAttribute("edit", true);
		return "add-project";
	}
	
	@RequestMapping(value = { "/edit-project-{id}" }, method = RequestMethod.POST)
	public String updateProject(@Valid Project project, BindingResult result,
			ModelMap model, @PathVariable String id, HttpServletRequest request, HttpSession session) throws Exception {
		
		projectService.updateProject(project);
		List<Client> clientsList = clientService.findAllClients();
		model.addAttribute("clientsList", clientsList);
		model.addAttribute("projects", projectService.findAllProjects());
		return "redirect:/view-projects";
	}
	
	@RequestMapping(value = { "/inactivate-project-{id}" }, method = RequestMethod.GET)
	public String inactivateProject(@PathVariable int id, HttpServletRequest request, ModelMap model, HttpSession session) {
		projectService.inactivateProjectById(id);
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
/*		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);*/
		model.addAttribute("projects", projectService.findAllProjects());
		return "redirect:/view-projects";
	}

	@RequestMapping(value = { "/add-module" }, method = RequestMethod.GET)
	public String viewAddModulePage(ModelMap model, HttpSession session) throws Exception {
		Module module = new Module();
		List<Project> projectsList = projectService.findAllProjects();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("module", module);
		model.addAttribute("projectsList", projectsList);
		return "add-module";
	}

	@RequestMapping(value = { "/add-module" }, method = RequestMethod.POST)
	public String addModule(@Valid Module module, BindingResult result, ModelMap model, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		Module newModule = new Module();
		List<Project> projectsList = projectService.findAllProjects();
		
		
		model.addAttribute("projectsList", projectsList);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);

		/*if(result.hasErrors()) {
			model.addAttribute("error", "An error occurred while creating the module.");
			return "add-module";
		}*/
		
		Module moduleByModuleName = moduleService.findByName(module.getName());
		if(moduleByModuleName != null) {
			model.addAttribute("error", "A module with the same name already exists.");
			return "add-module";
		}
		
		moduleService.saveModule(module);
		model.addAttribute("success", "Module successfully created.");
		model.addAttribute("module", newModule);
		return "add-module";
	}
	
	@RequestMapping(value = { "/view-modules" }, method = RequestMethod.GET)
	public String viewAllModulesPage(ModelMap model, HttpSession session) throws Exception {
		List<Module> modules = moduleService.findAllModules();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("modules", modules);
		return "view-modules";
	}
	
	@RequestMapping(value = { "/edit-module-{id}" }, method = RequestMethod.GET)
	public String editModulePage(@PathVariable int id, ModelMap model, HttpSession session) throws Exception {
		Module module = moduleService.findById(id);
		List<Project> projectsList = projectService.findAllProjects();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("module", module);
		model.addAttribute("projectsList", projectsList);
		model.addAttribute("edit", true);
		return "add-module";
	}
	
	@RequestMapping(value = { "/edit-module-{id}" }, method = RequestMethod.POST)
	public String updateModule(@Valid Module module, BindingResult result,
			ModelMap model, @PathVariable String id, HttpServletRequest request, HttpSession session) throws Exception {
		
		moduleService.updateModule(module);
		List<Project> projectsList = projectService.findAllProjects();
		model.addAttribute("projectsList", projectsList);
		model.addAttribute("modules", moduleService.findAllModules());
		return "redirect:/view-modules";
	}
	
	@RequestMapping(value = { "/inactivate-module-{id}" }, method = RequestMethod.GET)
	public String inactivateModule(@PathVariable int id, HttpServletRequest request, ModelMap model, HttpSession session) {
		moduleService.inactivateModuleById(id);
		model.addAttribute("modules", moduleService.findAllModules());
		return "redirect:/view-modules";
	}

	@RequestMapping(value = { "/add-client" }, method = RequestMethod.GET)
	public String viewAddClientPage(ModelMap model, HttpSession session) throws Exception {
		Client client = new Client();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("client", client);
		return "add-client";
	}

	@RequestMapping(value = { "/add-client" }, method = RequestMethod.POST)
	public String addClient(@Valid Client client, BindingResult result, ModelMap model, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		Client newClient = new Client();
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		/*if(result.hasErrors()) {
			model.addAttribute("error", "An error occurred while adding the client.");
			return "add-client";
		}*/
		Client clientByClientName = clientService.findByName(client.getName());
		if(clientByClientName != null) {
			model.addAttribute("error", "A client with the same name already exists.");
			return "add-client";
		}
		
		clientService.saveClient(client);
		model.addAttribute("success", "Client successfully added.");
		model.addAttribute("client", newClient);
		return "add-client";
	}
	
	@RequestMapping(value = { "/view-clients" }, method = RequestMethod.GET)
	public String viewAllClientsPage(ModelMap model, HttpSession session) throws Exception {
		List<Client> clients = clientService.findAllClients();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("clients", clients);
		return "view-clients";
	}
	
	@RequestMapping(value = { "/edit-client-{id}" }, method = RequestMethod.GET)
	public String editClientPage(@PathVariable int id, ModelMap model, HttpSession session) throws Exception {
		Client client = clientService.findById(id);
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("client", client);
		model.addAttribute("edit", true);
		return "add-client";
	}
	
	@RequestMapping(value = { "/edit-client-{id}" }, method = RequestMethod.POST)
	public String updateClient(@Valid Client client, BindingResult result,
			ModelMap model, @PathVariable String id, HttpServletRequest request, HttpSession session) throws Exception {

		
		clientService.updateClient(client);
		model.addAttribute("clients",clientService.findAllClients());
		return "redirect:/view-clients";
	}
	
	@RequestMapping(value = { "/inactivate-client-{id}" }, method = RequestMethod.GET)
	public String inactivateClient(@PathVariable int id, HttpServletRequest request, ModelMap model, HttpSession session) {
		
		clientService.inactivateClientById(id);
		model.addAttribute("clients",clientService.findAllClients());
		return "redirect:/view-clients";
	}

	@RequestMapping(value = { "/add-task" }, method = RequestMethod.GET)
	public String viewAddTaskPage(ModelMap model, HttpSession session) throws Exception {
		Task task = new Task();
		List<Project> projectsList = projectService.findAllProjects();
		//List<Module> modulesList = moduleService.findAllModules();
		List<User> usersList = userService.findAllUsers();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("task", task);
		model.addAttribute("projectsList", projectsList);
		//model.addAttribute("modulesList", modulesList);
		model.addAttribute("usersList", usersList);
		return "add-task";
	}

	@RequestMapping(value = { "/add-task" }, method = RequestMethod.POST)
	public String addTask(@Valid Task task, BindingResult result, ModelMap model, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		List<Project> projectsList = projectService.findAllProjects();
		List<User> usersList = userService.findAllUsers();
		Task newTask = new Task();
		
		model.addAttribute("projectsList", projectsList);
		model.addAttribute("usersList", usersList);
		if(((User) session.getAttribute("loggedInUserObject")).getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		/*if(result.hasErrors()) {
			model.addAttribute("error", "An error occurred while creating the task.");
			return "add-task";
		}*/
		
		Task taskByTaskName = taskService.findByName(task.getName());
		if(taskByTaskName != null) {
			model.addAttribute("error", "A task with the same name already exists.");
			return "add-task";
		}
		
		taskService.saveTask(task);
		model.addAttribute("success", "Task successfully created.");
		model.addAttribute("tasks", taskService.findAllTasks());
		model.addAttribute("task", newTask);
		return "add-task";
	}
	
	
	@RequestMapping(value = { "/view-tasks" }, method = RequestMethod.GET)
	public String viewAllTasksPage(ModelMap model, HttpSession session) throws Exception {
		List<Task> tasks = taskService.findAllTasks();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("tasks", tasks);
		return "view-tasks";
	}
	
	@RequestMapping(value = { "/edit-task-{id}" }, method = RequestMethod.GET)
	public String editTaskPage(@PathVariable int id, ModelMap model, HttpSession session) throws Exception {
		Task task = taskService.findById(id);
		List<Project> projectsList = projectService.findAllProjects();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("task", task);
		model.addAttribute("projectsList", projectsList);
		model.addAttribute("edit", true);
		return "add-task";
	}
	
	@RequestMapping(value = { "/edit-task-{id}" }, method = RequestMethod.POST)
	public String updateTask(@Valid Task task, BindingResult result,
			ModelMap model, @PathVariable String id, HttpServletRequest request, HttpSession session) throws Exception {
		
		taskService.updateTask(task);
		model.addAttribute("tasks",taskService.findAllTasks());
		return "redirect:/view-tasks";
	}
	
	@RequestMapping(value = { "/inactivate-task-{id}" }, method = RequestMethod.GET)
	public String inactivateTask(@PathVariable int id, HttpServletRequest request, ModelMap model, HttpSession session) {
		taskService.inactivateTaskById(id);
		/*User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		User userByLoggedInUserId = userService.findById(user.getId());
		if(userByLoggedInUserId.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("success", "Task successfully activated/inactivated.");*/
		model.addAttribute("tasks",taskService.findAllTasks());
		return "redirect:/view-tasks";
	}
	
	@RequestMapping(value = { "/allocate-resource" }, method = RequestMethod.GET)
	public String viewResourceAllocationPage(ModelMap model, HttpSession session) throws Exception {
		Task task = new Task();
		List<Project> projectsList = projectService.findAllProjects();
		List<User> usersList = userService.findAllUsers();
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("task", task);
		model.addAttribute("projectsList", projectsList);
		model.addAttribute("usersList", usersList);
		return "allocate-resource";
	}
	
	@RequestMapping(value = { "/allocate-resource" }, method = RequestMethod.POST)
	public ResponseEntity<?> allocateResource(@RequestBody Map<String, Object> selectedUsersForTaskMap, BindingResult result, ModelMap model, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		List<Project> projectsList = projectService.findAllProjects();
		List<User> usersList = userService.findAllUsers();
		Task newTask = new Task();
		
		String taskId = (String) selectedUsersForTaskMap.get("taskId"); 
		List<String> selectedUsersForTask = (List<String>) selectedUsersForTaskMap.get("usersList"); 
		
		model.addAttribute("projectsList", projectsList);
		model.addAttribute("usersList", usersList);
		
		/*if(result.hasErrors()) {
			//model.addAttribute("error", "An error occurred while allocating the resources.");
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}*/
		
		taskService.updateUsersForTask(Integer.parseInt(taskId), selectedUsersForTask);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		//model.addAttribute("success", "Resources successfully allocated.");
		model.addAttribute("task", taskService.findById(Integer.parseInt(taskId)));
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/team-timesheets" }, method = RequestMethod.GET)
	public String viewTeamSubmittedTimesheetsListPage(ModelMap model, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		String isUserAdmin = "No";
		List<UserSubmittedTimeLog> userSubmittedTimeLogsListByStatusAndUserRole = timeEntryService.findAllUserSubmittedTimeLogsByStatusesAsSubmittedAndResubmittedAndApprovedAndUserRole(isUserAdmin);
		model.addAttribute("userSubmittedTimeLogsListByStatusAndUserRole", userSubmittedTimeLogsListByStatusAndUserRole);
		return "team-timesheets";
	}

	@RequestMapping(value = { "/time-approval-{userId}_{logDate}" }, method = RequestMethod.GET) //change to @valid, remove @pathvariable
	public String viewTimeSheetPageForApproval(@PathVariable int userId, @PathVariable String logDate, ModelMap model, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal()); model.addAttribute("loggedInUserName", user.getFirstName() + " " + user.getLastName()); model.addAttribute("loggedInUserObject", user);
		UserSubmittedLogsListDTO userSubmittedTimeLogsListDTO = new UserSubmittedLogsListDTO();
		List<UserSubmittedTimeLog> userSubmittedTimeLogsListByUserIdAndLogDate = timeEntryService.findAllUserSubmittedTimeLogsByUserIdAndLogDate(userId, logDate);
		User timeSheetUser = userService.findById(userId);
		
		if(userSubmittedTimeLogsListByUserIdAndLogDate != null) {
			userSubmittedTimeLogsListDTO.setUserSubmittedTimeLogsList(userSubmittedTimeLogsListByUserIdAndLogDate);
		}
		
		MultiValueMap<Integer, Task> tasksForCurrentUserPerModuleMap = taskService.findAllTasksByUser(userId);
		List<String> selectedWeekDatesList = timeEntryService.getSelectedWeekDates(logDate);
		
		if(user.getIsAdmin().equals("Yes"))
			model.addAttribute("isLoggedInUserAdmin", true);
		model.addAttribute("loggedInUser", user.getId());
		model.addAttribute("userTimesheetBelongsTo", userId); //incase of admin viewing other emp's timesheet
		model.addAttribute("userName" , timeSheetUser.getFirstName() + " " + timeSheetUser.getLastName());
		model.addAttribute("isAdminViewingTimesheet", true);
		model.addAttribute("selectedWeekDatesList", selectedWeekDatesList);
		model.addAttribute("tasksForCurrentUserPerModuleMap", tasksForCurrentUserPerModuleMap);
		model.addAttribute("userSubmittedTimeLogsList", userSubmittedTimeLogsListDTO);
		model.addAttribute("totalPtoHoursPerWeek",userSubmittedTimeLogsListByUserIdAndLogDate.size() > 0 ?  userSubmittedTimeLogsListByUserIdAndLogDate.get(0).getTotalPtoHoursPerWeek() : "0.0,0.0,0.0,0.0,0.0,0.0,0.0");
		return "time-entry";
	}

	@RequestMapping(value = { "/changeTimesheetStatus" }, method = RequestMethod.POST)
	public ResponseEntity<?> changeTimesheetStatus(@RequestBody Map<String, String> timesheetStatusMap, HttpSession session) throws Exception {
		User user = userService.findByUsername(getPrincipal()); 
		String userId = timesheetStatusMap.get("userId");
		String logDate = timesheetStatusMap.get("logDate");
		String status = timesheetStatusMap.get("status");
		boolean statusUpdatedSuccessfully = timeEntryService.updateUserSubmittedTimeLogsStatus(userId, logDate, status);
		User loggedInAdminUser = userService.findById(user.getId());
		User userToSendEmailTo = userService.findById(Integer.parseInt(userId));
		Map<String, String> emailContentMap = new HashMap<>();
		if(statusUpdatedSuccessfully) {
			if(status.equals("Reopened")) {
				emailContentMap.put("emailTo", userToSendEmailTo.getUsername());
				emailContentMap.put("subject", "INSTEP Time Management - Request for Time-sheet Re-submission ");
				emailContentMap.put("text", "Dear " + userToSendEmailTo.getFirstName() + " "+ userToSendEmailTo.getLastName() + ",\n" + loggedInAdminUser.getFirstName() + " " + loggedInAdminUser.getLastName() + " has requested you to resubmit your timesheet for the week ending " + logDate + ".\nRegards,\nINSTEP Time App Admin ");
				emailService.sendNotificationViaEmail(emailContentMap);
			}
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		else
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}
	
	/**
	 * This method will provide user profiles list to views
	 */
	@ModelAttribute("roles")
	public List<UserProfile> initializeProfiles() {
		return userProfileService.findAll();
	}
	
	/**
	 * This method returns the principal[user-name] of logged-in user.
	 */
	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}

	/**
	 * This method returns true if users is already authenticated [logged-in],
	 * else false.
	 */
	private boolean isCurrentAuthenticationAnonymous() {
		final Authentication authentication = SecurityContextHolder
				.getContext().getAuthentication();
		return authenticationTrustResolver.isAnonymous(authentication);
	}
	
}
