package com.instep.tma.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.instep.tma.model.Module;
import com.instep.tma.model.Project;
import com.instep.tma.model.Task;
import com.instep.tma.service.ModuleService;
import com.instep.tma.service.TaskService;

@Component
public class TaskConverter implements Converter<String, Task>{
	
	static final Logger logger = LoggerFactory.getLogger(TaskConverter.class);
	
	@Autowired
	TaskService taskService;

	@Override
	public Task convert(String element) {
		Integer id = Integer.parseInt(element);
		Task task = taskService.findById(id);
		logger.info("Task : {}",task);
		return task;
}

}
