package com.instep.tma.converter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.instep.tma.model.UserProfile;
import com.instep.tma.service.UserProfileService;

/**
 * A converter class used in views to map id's to actual userProfile objects.
 */
@Component
public class UserProfileConverter implements Converter<String, UserProfile>{

	static final Logger logger = Logger.getLogger(UserProfileConverter.class);
	
	@Autowired
	UserProfileService userProfileService;

	/**
	 * Gets UserProfile by Id	
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	public UserProfile convert(String element) {
		
		//Integer id = Integer.parseInt((String)element);
		if(element.equalsIgnoreCase("null")){
			return null;
		}
		Integer id = Integer.parseInt(element);
		UserProfile profile= userProfileService.findById(id);
		logger.info("Profile : {}" + profile);
		return profile;
		
	}
	
}