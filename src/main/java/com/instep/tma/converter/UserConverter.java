package com.instep.tma.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.instep.tma.model.Client;
import com.instep.tma.model.User;
import com.instep.tma.service.UserService;

/**
 * A converter class used in views to map id's to actual client objects.
 */
@Component
public class UserConverter implements Converter<String, User> {

	static final Logger logger = LoggerFactory.getLogger(UserConverter.class);

	@Autowired
	UserService userService;
	  		
	/**
	 * Gets User by Id
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	public User convert(String element) {

		Integer id = Integer.parseInt(element);
		User user = userService.findById(id);
		logger.info("User : {}", user);
		return user;

	}

}
