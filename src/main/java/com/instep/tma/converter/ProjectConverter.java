package com.instep.tma.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.instep.tma.model.Project;
import com.instep.tma.service.ProjectService;

@Component
public class ProjectConverter implements Converter<String, Project>{
	
	static final Logger logger = LoggerFactory.getLogger(ProjectConverter.class);
	
	@Autowired
	ProjectService projectService;

	@Override
	public Project convert(String element) {
		Integer id = Integer.parseInt(element);
		Project project = projectService.findById(id);
		logger.info("Project : {}",project);
		return project;
}

}
