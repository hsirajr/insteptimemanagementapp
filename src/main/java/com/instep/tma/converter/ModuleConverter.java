package com.instep.tma.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.instep.tma.model.Module;
import com.instep.tma.model.Project;
import com.instep.tma.service.ModuleService;

@Component
public class ModuleConverter implements Converter<String, Module>{
	
	static final Logger logger = LoggerFactory.getLogger(ModuleConverter.class);
	
	@Autowired
	ModuleService moduleService;

	@Override
	public Module convert(String element) {
		Integer id = Integer.parseInt(element);
		Module module = moduleService.findById(id);
		logger.info("Module : {}",module);
		return module;
}

}
