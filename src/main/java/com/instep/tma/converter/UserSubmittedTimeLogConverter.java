package com.instep.tma.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.instep.tma.model.UserSubmittedTimeLog;
import com.instep.tma.service.ClientService;
import com.instep.tma.service.TimeEntryService;

/**
 * A converter class used in views to map id's to actual client objects.
 */
@Component
public class UserSubmittedTimeLogConverter implements Converter<String, UserSubmittedTimeLog>{

	static final Logger logger = LoggerFactory.getLogger(UserSubmittedTimeLogConverter.class);
	
	@Autowired
	TimeEntryService timeEntryService;

	/**
	 * Gets UserSubmittedTimeLog by Id
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	public UserSubmittedTimeLog convert(String element) {
		
		Integer id = Integer.parseInt(element);
		UserSubmittedTimeLog userSubmittedTimeLog = timeEntryService.findById(id);
		logger.info("UserSubmittedTimeLog : {}",userSubmittedTimeLog);
		return userSubmittedTimeLog;
		
	}
	
}



