package com.instep.tma.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.instep.tma.model.Client;
import com.instep.tma.service.ClientService;

/**
 * A converter class used in views to map id's to actual client objects.
 */
@Component
public class ClientConverter implements Converter<String, Client>{

	static final Logger logger = LoggerFactory.getLogger(ClientConverter.class);
	
	@Autowired
	ClientService clientService;

	/**
	 * Gets Client by Id
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	public Client convert(String element) {
		
		Integer id = Integer.parseInt(element);
		Client client = clientService.findById(id);
		logger.info("Client : {}",client);
		return client;
		
	}
	
}



