package com.instep.tma.model;

import java.io.Serializable;

public enum UserProfileType implements Serializable{
	STAFF("STAFF"),
	LEAD("LEAD"),
	PROJECT_ADMIN("PROJECT_ADMIN"),
	SUPER_ADMIN("SUPER_ADMIN");
	
	String userProfileType;
	
	private UserProfileType(String userProfileType){
		this.userProfileType = userProfileType;
	}
	
	public String getUserProfileType(){
		return userProfileType;
	}
	
}
