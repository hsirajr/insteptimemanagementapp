package com.instep.tma.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Audited
@Table(name = "users")
public class User {
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotAudited	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ")
	@SequenceGenerator(name = "USER_SEQ", sequenceName = "USER_SEQ", allocationSize = 1)
	private Integer id;
	
	@NotEmpty
	@Column(name = "username", unique = true, nullable = false)
	private String username;
	
	@NotAudited
	@Column(name = "password")
	private String password;

	@NotEmpty
	@Column(name = "first_name", nullable = false)
	private String firstName;

	@NotEmpty
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "lead_id", nullable = false) //rename to lead_id, change type to integer
	private int leadId;
	
	@NotEmpty
	@Column(name = "is_admin", nullable = false)
	private String isAdmin;
	
	@NotEmpty
	@Column(name = "status", nullable = false)
	private String status = "Active";
	
	@NotAudited
	@ManyToMany(mappedBy="users", cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private List<Task> tasks = new ArrayList<Task>();
	
	/*@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Task> tasks = new ArrayList<Task>();*/
	
	@NotAudited
	@OneToMany(mappedBy = "user")
	@JsonIgnore
	private List<DeviceTimeLog> deviceTimeLogs = new ArrayList<DeviceTimeLog>();
	
	@NotAudited
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user", orphanRemoval = true)
	@JsonIgnore
	private List<UserSubmittedTimeLog> userSubmittedTimeLogs = new ArrayList<UserSubmittedTimeLog>();
	
	@NotAudited
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_user_profile", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "user_profile_id") })
	private Set<UserProfile> userProfiles = new HashSet<UserProfile>();

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "added_date")
	private Date addedDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	private Date updatedDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getLeadId() {
		return leadId;
	}

	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}

	public String getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public List<DeviceTimeLog> getDeviceTimeLogs() {
		return deviceTimeLogs;
	}

	public void setDeviceTimeLogs(List<DeviceTimeLog> deviceTimeLogs) {
		this.deviceTimeLogs = deviceTimeLogs;
	}

	public List<UserSubmittedTimeLog> getUserSubmittedTimeLogs() {
		return userSubmittedTimeLogs;
	}

	public void setUserSubmittedTimeLogs(List<UserSubmittedTimeLog> userSubmittedTimeLogs) {
		this.userSubmittedTimeLogs = userSubmittedTimeLogs;
	}

	
	public Set<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	public void setUserProfiles(Set<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {	
		this.addedDate = addedDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", leadId=" + leadId + ", isAdmin=" + isAdmin + ", status=" + status 
				+ ", addedDate=" + addedDate + ", updatedDate=" + updatedDate + "]";
	}

	
	
}
