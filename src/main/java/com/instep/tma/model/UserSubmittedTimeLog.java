package com.instep.tma.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Audited
@Table(name = "user_submitted_time_log")
public class UserSubmittedTimeLog {
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotAudited
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SUBMITTED_TIME_LOG_SEQ")
	@SequenceGenerator(name = "USER_SUBMITTED_TIME_LOG_SEQ", sequenceName = "USER_SUBMITTED_TIME_LOG_SEQ", allocationSize = 1)
	private Integer id;
	
	@NotEmpty
	@Column(name = "log_date", nullable = false) //good to change to week-identifier-date
	private String date;
	
	@Column(name = "hours_per_day_per_week")
	private String hoursPerDayPerWeek;
	
	@NotEmpty
	@Column(name = "total_hours_per_week")
	private String totalHoursPerWeek;
	
	@Column(name = "total_pto_hours_per_week")
	private String totalPtoHoursPerWeek;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "task_id", nullable = false)
	private Task task;
	
	@Column(name = "status")
	private String status = "Open";
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "added_date")
	private Date addedDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	private Date updatedDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHoursPerDayPerWeek() {
		return hoursPerDayPerWeek;
	}

	public void setHoursPerDayPerWeek(String hoursPerDayPerWeek) {
		this.hoursPerDayPerWeek = hoursPerDayPerWeek;
	}

	public String getTotalHoursPerWeek() {
		return totalHoursPerWeek;
	}

	public void setTotalHoursPerWeek(String totalHoursPerWeek) {
		this.totalHoursPerWeek = totalHoursPerWeek;
	}
	
	public String getTotalPtoHoursPerWeek() {
		return totalPtoHoursPerWeek;
	}

	public void setTotalPtoHoursPerWeek(String totalPtoHoursPerWeek) {
		this.totalPtoHoursPerWeek = totalPtoHoursPerWeek;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "UserSubmittedTimeLog [id=" + id + ", date=" + date + ", hoursPerDayPerWeek=" + hoursPerDayPerWeek
				+ ", totalHoursPerWeek=" + totalHoursPerWeek + ", user=" + user + ", task=" + task + ", status=" + status + "]";
	}

	
	
}
