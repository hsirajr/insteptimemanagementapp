package com.instep.tma.configuration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import com.instep.tma.converter.ClientConverter;
import com.instep.tma.converter.ModuleConverter;
import com.instep.tma.converter.ProjectConverter;
import com.instep.tma.converter.TaskConverter;
import com.instep.tma.converter.UserConverter;
import com.instep.tma.converter.UserProfileConverter;
import com.instep.tma.converter.UserSubmittedTimeLogConverter;
import com.instep.tma.util.SecurityValidations;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.instep.tma")
@PropertySource(value = "classpath:application.properties")
public class AppConfig extends WebMvcConfigurerAdapter {

	@Autowired
	UserConverter userConverter;
	
	@Autowired
	ClientConverter clientConverter;
	
	@Autowired
	ModuleConverter moduleConverter;
	
	@Autowired
	ProjectConverter projectConverter;
	
	@Autowired
	TaskConverter taskConverter;
	
	@Autowired
	UserSubmittedTimeLogConverter userSubmittedTimeLogConverter;
	
	@Autowired
	UserProfileConverter userProfileConverter;

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		/*InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		registry.viewResolver(viewResolver);*/

		TilesViewResolver viewResolver = new TilesViewResolver();
		registry.viewResolver(viewResolver);

	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/static/**").addResourceLocations("/static/");
        registry.addResourceHandler("/static/css/**").addResourceLocations("/static/css/");
        registry.addResourceHandler("/static/fonts/**").addResourceLocations("/static/fonts/");
        registry.addResourceHandler("/static/scripts/**").addResourceLocations("/static/scripts/");
        registry.addResourceHandler("/static/images/**").addResourceLocations("/static/images/");
	}

	@Bean
	public SecurityValidations securityValidations() {
		return new SecurityValidations();
	}

	@Bean
	public TilesConfigurer tilesConfigurer(){
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(new String[] {"/WEB-INF/views/**/tiles.xml"});
		tilesConfigurer.setCheckRefresh(true);
		return tilesConfigurer;
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(userConverter);
		registry.addConverter(clientConverter);
		registry.addConverter(moduleConverter);
		registry.addConverter(projectConverter);
		registry.addConverter(taskConverter);
		registry.addConverter(userSubmittedTimeLogConverter);
		registry.addConverter(userProfileConverter);
	}
	
	@Override
	public void configureMessageConverters(
			List<HttpMessageConverter<?>> converters) {
		converters.add(new MappingJackson2HttpMessageConverter());
	}

}
