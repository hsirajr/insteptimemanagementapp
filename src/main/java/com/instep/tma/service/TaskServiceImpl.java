package com.instep.tma.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.map.MultiValueMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.instep.tma.dao.TaskDao;
import com.instep.tma.model.Task;
import com.instep.tma.model.User;

@Service("taskService")
@Transactional
public class TaskServiceImpl implements TaskService {

	@Autowired
	TaskDao dao;
	@Autowired
	UserService userService;
	
	@Override
	public List<Task> findAllTasks() {
		return dao.findAllTasks();
	}

	@Override
	public void saveTask(Task task) {
		dao.saveTask(task);
	}

	@Override
	public Task findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public MultiValueMap<Integer, Task> findAllTasksByUser(int userId) {
		List<Task> tasksListByUser = dao.findAllTasksByUser(userId);
		//Map<Integer, Task> tasksMapByUserPerModule = new HashMap<>();
		MultiValueMap<Integer, Task> tasksMapByUserPerModule = new MultiValueMap<Integer, Task>();
		for(Task task : tasksListByUser) {
			tasksMapByUserPerModule.put(task.getModule().getId(), task);
		}
		return tasksMapByUserPerModule;
	}

	@Override
	public Task findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public List<Task> findAllTasksByModuleId(int moduleId) {
		return dao.findAllTasksByModuleId(moduleId);
	}

	@Override
	public void updateUsersForTask(int taskId, List<String> selectedUsersForTask) {
		Task task = findById(taskId);
		List<User> users = new ArrayList<User>();
		User user = new User();
		for(String selectedUserForTask : selectedUsersForTask) {
			user = userService.findById(Integer.parseInt(selectedUserForTask));
			users.add(user);
		}
		if(task != null) {
			task.setUsers(users);
		}
	}

	@Override
	public void updateTask(Task task) {
		Task entity = findById(task.getId());
		if(entity != null) {
			entity.setName(task.getName());
			entity.setDescription(task.getDescription());
			entity.setModule(task.getModule());
		}
	}

	@Override
	public void deleteTaskById(int id) {
		dao.deleteById(id);
	}

	@Override
	public void inactivateTaskById(int id) {
		Task task = findById(id);
		if(task != null && task.getStatus().equals("Active")) {
			task.setStatus("Inactive");
		} else{
			task.setStatus("Active");
		}
		
	}

	
	

}
