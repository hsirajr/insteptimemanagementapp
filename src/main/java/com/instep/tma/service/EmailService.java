package com.instep.tma.service;

import java.util.Map;

public interface EmailService {
	
	void sendNotificationViaEmail(Map<String, String> emailContentMap);

}
