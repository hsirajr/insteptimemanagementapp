package com.instep.tma.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.instep.tma.dao.ModuleDao;
import com.instep.tma.model.Module;
import com.instep.tma.model.User;

@Service("moduleService")
@Transactional
public class ModuleServiceImpl implements ModuleService {

	@Autowired
	ModuleDao dao;
	
	@Override
	public List<Module> findAllModules() {
		return dao.findAllModules();
	}

	@Override
	public void saveModule(Module module) {
		dao.saveModule(module);
	}

	@Override
	public Module findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public Module findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public List<Module> findAllModulesByProjectId(int projectId) {
		return dao.findAllModulesByProjectId(projectId);
	}

	@Override
	public void updateModule(Module module) {
		Module entity = findById(module.getId());
		if(entity != null) {
			entity.setName(module.getName());
			entity.setDescription(module.getDescription());
			entity.setProject(module.getProject());
		}
	}

	@Override
	public void deleteModuleById(int id) {
		dao.deleteById(id);
	}

	@Override
	public void inactivateModuleById(int id) {
		Module module = findById(id);
		if(module != null && module.getStatus().equals("Active")) {
			module.setStatus("Inactive");
		} else{
			module.setStatus("Active");
		}
		
	}
	
	

}
