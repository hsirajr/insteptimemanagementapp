package com.instep.tma.service;

import java.util.List;

import com.instep.tma.model.Project;

public interface ProjectService {

	List<Project> findAllProjects();

	void saveProject(Project project);

	Project findById(Integer id);

	Project findByName(String name);

	void updateProject(Project project);

	void deleteProjectById(int id);

	void inactivateProjectById(int id);
	
}
