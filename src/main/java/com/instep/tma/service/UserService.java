package com.instep.tma.service;

import java.util.List;

import com.instep.tma.model.User;

public interface UserService {

	User findByLoginCredentials(String username, String password) throws Exception;
	
	void saveUser(User user);

	List<User> findAllUsers();

	User findById(Integer id);
	
	User findByUsername(String username);

	boolean savePasswordForNewUser(String username, String password) throws Exception;

	List<User> findAllAllocatedUsersByTaskId(int taskId);
	
	List<User> findAllNonAllocatedUsersByTaskId(int taskId);

	void updateUser(User user) throws Exception;

	void inactivateUserById(int id);

	List<User> findAllUsersByAdminRole();

	User resetPassword(int id);
	
}
