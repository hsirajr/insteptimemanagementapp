package com.instep.tma.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.MultiValueMap;

import com.instep.tma.model.Task;

public interface TaskService {

	List<Task> findAllTasks();

	void saveTask(Task task);

	Task findById(Integer id);
	
	MultiValueMap<Integer, Task> findAllTasksByUser(int userId);

	Task findByName(String name);

	List<Task> findAllTasksByModuleId(int moduleId);

	void updateUsersForTask(int taskId, List<String> selectedUsersForTask);

	void updateTask(Task task);

	void deleteTaskById(int id);

	void inactivateTaskById(int id);
	
}
