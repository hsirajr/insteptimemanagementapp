package com.instep.tma.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.instep.tma.dao.ProjectDao;
import com.instep.tma.model.Project;

@Service("projectService")
@Transactional
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	ProjectDao dao;
	
	@Override
	public List<Project> findAllProjects() {
		return dao.findAllProjects();
	}

	@Override
	public void saveProject(Project project) {
		dao.saveProject(project);
	}

	@Override
	public Project findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public Project findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void updateProject(Project project) {
		Project entity = findById(project.getId());
		if(entity != null) {
			entity.setName(project.getName());
			entity.setDescription(project.getDescription());
			entity.setClient(project.getClient());
			//entity.setModules(project.getModules());
		}
	}

	@Override
	public void deleteProjectById(int id) {
		dao.deleteById(id);
	}

	@Override
	public void inactivateProjectById(int id) {
		Project project = findById(id);
		if(project != null && project.getStatus().equals("Active")) {
			project.setStatus("Inactive");
		} else{
			project.setStatus("Active");	
		}
	}
	
	

}
