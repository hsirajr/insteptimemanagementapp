package com.instep.tma.service;

import java.util.List;

import com.instep.tma.model.Module;

public interface ModuleService {

	List<Module> findAllModules();

	void saveModule(Module module);

	Module findById(Integer id);

	Module findByName(String name);

	List<Module> findAllModulesByProjectId(int projectId);

	void updateModule(Module module);

	void deleteModuleById(int id);

	void inactivateModuleById(int id);
	
}
