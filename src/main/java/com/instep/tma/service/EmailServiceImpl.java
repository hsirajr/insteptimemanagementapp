package com.instep.tma.service;

import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@PropertySource( value= { "classpath:application.properties" } )
@Service("emailService")
public class EmailServiceImpl implements EmailService{
	
	@Autowired
	private Environment environment;

	public void sendNotificationViaEmail(Map<String, String> emailContentMap) {
		final String smtpAuthUserName = environment.getRequiredProperty("smtp.user.username");
		final String smtpAuthPassword = environment.getRequiredProperty("smtp.user.password");
		String emailFrom = environment.getRequiredProperty("smtp.user.username");
		String emailTo = emailContentMap.get("emailTo"); //emp's username here
		Authenticator authenticator = new Authenticator()
		{
			@Override	
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return  new PasswordAuthentication(smtpAuthUserName, smtpAuthPassword);
			}
		};
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.host", environment.getRequiredProperty("mail.smtp.host"));
		properties.setProperty("mail.smtp.port", environment.getRequiredProperty("mail.smtp.port"));
		properties.setProperty("mail.smtp.auth", environment.getRequiredProperty("mail.smtp.auth"));
		properties.setProperty("mail.smtp.starttls.enable", environment.getRequiredProperty("mail.smtp.starttls.enable"));
		Session session = Session.getInstance( properties, authenticator );
		try
		{
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailFrom));
			InternetAddress[] to = {new InternetAddress(emailTo)};
			message.setRecipients(Message.RecipientType.TO, to);
			message.setSubject(emailContentMap.get("subject"));
			message.setText(emailContentMap.get("text"));
			Transport.send(message);
			System.out.println("-------Email successfully sent");
		}
		catch (MessagingException exception)
		{
			exception.printStackTrace();
		}

	}

}
