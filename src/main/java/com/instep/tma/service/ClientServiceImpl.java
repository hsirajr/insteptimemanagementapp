package com.instep.tma.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.instep.tma.dao.ClientDao;
import com.instep.tma.model.Client;
import com.instep.tma.model.User;

@Service("clientService")
@Transactional
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientDao dao;
	
	@Override
	public Client findById(int id) {
		return dao.findById(id);
	}
	
	@Override
	public List<Client> findAllClients() {
		return dao.findAllClients();
	}

	@Override
	public void saveClient(Client client) {
		dao.saveClient(client);
	}

	@Override
	public Client findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public void updateClient(Client client) {
		Client entity = findById(client.getId());
		if(entity != null) {
			entity.setName(client.getName());
			entity.setDescription(client.getDescription());
		}
	}

	@Override
	public void deleteClientById(int id) {
		dao.deleteById(id);
	}

	@Override
	public void inactivateClientById(int id) {
		Client client = findById(id);
		if(client != null && client.getStatus().equals("Active")) {
			client.setStatus("Inactive");
		} else{
			client.setStatus("Active");
		}
		
	}


	

}
