package com.instep.tma.service;

import java.util.List;

import com.instep.tma.model.Client;

public interface ClientService {
	
	Client findById(int id);

	List<Client> findAllClients();

	void saveClient(Client client);

	Client findByName(String name);

	void updateClient(Client client);

	void deleteClientById(int id);

	void inactivateClientById(int id);
	
}
