package com.instep.tma.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.instep.tma.dao.UserDao;
import com.instep.tma.model.User;
import com.instep.tma.util.SecurityValidations;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao dao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public User findByLoginCredentials(String username, String password) throws Exception {
		return dao.findByLoginCredentials(username, password);
	}

	@Override
	public void saveUser(User user) {
		dao.saveUser(user);
	}

	@Override
	public List<User> findAllUsers() {
		return dao.findAllUsers();
	}

	@Override
	public User findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public User findByUsername(String username) {
		return dao.findByUsername(username);
	}

	@Override
	public boolean savePasswordForNewUser(String username, String password) throws Exception {
		return dao.savePasswordForNewUser(username, password);
	}

	@Override
	public List<User> findAllAllocatedUsersByTaskId(int taskId) {
		return dao.findAllAllocatedUsersByTaskId(taskId);
	}

	@Override
	public List<User> findAllNonAllocatedUsersByTaskId(int taskId) {
		return dao.findAllNonAllocatedUsersByTaskId(taskId);
	}

	@Override
	public void updateUser(User user) throws Exception {
		User entity = findById(user.getId());
		if(entity != null) {
			entity.setFirstName(user.getFirstName());
			entity.setLastName(user.getLastName());
			if(user.getPassword() !=  null){
				entity.setPassword(passwordEncoder.encode(user.getPassword()));
			}
			entity.setLeadId(user.getLeadId());
			entity.setIsAdmin(user.getIsAdmin());
			entity.setStatus(user.getStatus());
			entity.setUserProfiles(user.getUserProfiles());
		}
	}

	@Override
	public void inactivateUserById(int id) {
		User user = findById(id);
		if(user != null && user.getStatus().equals("Active")) {
			user.setStatus("Inactive");
		} else{
			user.setStatus("Active");
		}
	}

	@Override
	public List<User> findAllUsersByAdminRole() {
		return dao.findAllUsersByAdminRole();
	}
	
	@Override
	public User resetPassword(int id) {
		User user = findById(id);
		if(user != null) {
			user.setPassword("");
		}
		return user;
	}

}
