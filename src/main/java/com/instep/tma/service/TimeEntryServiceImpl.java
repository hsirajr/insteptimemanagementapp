package com.instep.tma.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections4.map.MultiValueMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.instep.tma.dao.TimeEntryDao;
import com.instep.tma.dto.UserSubmittedLogsListDTO;
import com.instep.tma.model.User;
import com.instep.tma.model.UserSubmittedTimeLog;

import oracle.net.aso.s;

@Service("timeEntryService")
@Transactional
public class TimeEntryServiceImpl implements TimeEntryService {

	@Autowired
	private TimeEntryDao dao;

	@Override
	public UserSubmittedTimeLog findById(Integer id) {
		return dao.findById(id);
	}

	@Override
	public MultiValueMap<String, List<Date>> getPreviousFiveWeeks() throws ParseException {
		MultiValueMap<String, List<Date>> previousFiveWeeksMap = new MultiValueMap<String, List<Date>>();
		Calendar calendar = GregorianCalendar.getInstance();

		// Set the calendar to friday of the current week
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		// Print dates of the current week starting on Monday
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		String startDate = "", endDate = "";
		int i = 0;
		while(i < 5) {
			//calendar.add(Calendar.DATE, - 7 * i);
			endDate = dateFormat.format(calendar.getTime());
			calendar.add(Calendar.DATE, - 7);
			startDate = dateFormat.format(calendar.getTime());

			Date sDate = dateFormat.parse(startDate);
			Date eDate = dateFormat.parse(endDate);

			long interval = 24*1000 * 60 * 60;
			long endTime = eDate.getTime() ; 
			long curTime = sDate.getTime();
			while (curTime <= endTime) {
				curTime += interval;
				previousFiveWeeksMap.put("week-" + i, dateFormat.format(new Date(curTime)));
			}
			i++;
		}
		return previousFiveWeeksMap;	
	}

	@Override
	public MultiValueMap<String, List<Date>> getCurrentWeekAndUpcomingFourWeeks() throws ParseException {
		MultiValueMap<String, List<Date>> currentWeekAndUpcomingFiveWeeksMap = new MultiValueMap<String, List<Date>>();
		Calendar calendar = GregorianCalendar.getInstance();

		// Set the calendar to friday of the current week
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		// Print dates of the current week starting on Monday
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		String startDate = "", endDate = "";
		int i = 0;
		while(i < 6) {
			//calendar.add(Calendar.DATE, - 7 * i);
			startDate = dateFormat.format(calendar.getTime());
			calendar.add(Calendar.DATE, 7);
			endDate = dateFormat.format(calendar.getTime());

			Date sDate = dateFormat.parse(startDate);
			Date eDate = dateFormat.parse(endDate);

			long interval = 24*1000 * 60 * 60;
			long endTime = eDate.getTime() ; 
			long curTime = sDate.getTime();
			while (curTime <= endTime) {
				curTime += interval;
				currentWeekAndUpcomingFiveWeeksMap.put("week-" + i, dateFormat.format(new Date(curTime)));
			}
			i++;
		}
		return currentWeekAndUpcomingFiveWeeksMap;	
	}

	@Override
	public boolean saveUserSubmittedLog(UserSubmittedLogsListDTO userSubmittedTimeLogsListDTO) {
		List<UserSubmittedTimeLog> userSubmittedTimeLogsList = userSubmittedTimeLogsListDTO.getUserSubmittedTimeLogsList();
		String totalPtoHoursPerWeek = userSubmittedTimeLogsListDTO.getTotalPtoHoursPerWeek().substring(0, userSubmittedTimeLogsListDTO.getTotalPtoHoursPerWeek().length()-1);
		UserSubmittedTimeLog userSubmittedTimeLogByUserIdAndTaskIdAndLogDate = new UserSubmittedTimeLog();
		if(!userSubmittedTimeLogsList.isEmpty()) {
			for(UserSubmittedTimeLog userSubmittedTimeLog : userSubmittedTimeLogsList) {
				userSubmittedTimeLogByUserIdAndTaskIdAndLogDate = findAllUserSubmittedTimeLogsByUserIdAndTaskIdAndLogDate(userSubmittedTimeLog.getUser().getId(), userSubmittedTimeLog.getTask().getId(), userSubmittedTimeLog.getDate());
				if(userSubmittedTimeLogByUserIdAndTaskIdAndLogDate != null){
					userSubmittedTimeLog.setTotalPtoHoursPerWeek(totalPtoHoursPerWeek);
					update(userSubmittedTimeLog);
				} else {
					userSubmittedTimeLog.setTotalPtoHoursPerWeek(totalPtoHoursPerWeek);
					dao.save(userSubmittedTimeLog);
				}
			}
			return true;
		}
		return false;
	}

	private void update(UserSubmittedTimeLog userSubmittedTimeLog) {
		UserSubmittedTimeLog entity = dao.findById(userSubmittedTimeLog.getId());
		if(entity != null) {
			entity.setDate(userSubmittedTimeLog.getDate());
			entity.setHoursPerDayPerWeek(userSubmittedTimeLog.getHoursPerDayPerWeek());
			entity.setTotalHoursPerWeek(userSubmittedTimeLog.getTotalHoursPerWeek());
			entity.setTotalPtoHoursPerWeek(userSubmittedTimeLog.getTotalPtoHoursPerWeek());
			entity.setUser(userSubmittedTimeLog.getUser());
			entity.setTask(userSubmittedTimeLog.getTask());
			entity.setStatus(userSubmittedTimeLog.getStatus());
		}
	}

	@Override
	public List<UserSubmittedTimeLog> findAllUserSubmittedTimeLogsByUserIdAndLogDate(int userId, String logDate) {
		return dao.findAllUserSubmittedTimeLogsByUserIdAndLogDate(userId, logDate);
	}

	@Override
	public UserSubmittedTimeLog findAllUserSubmittedTimeLogsByUserIdAndTaskIdAndLogDate(int userId, int taskId, String logDate) {
		return dao.findAllUserSubmittedTimeLogsByUserIdAndTaskIdAndLogDate(userId, taskId, logDate);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getPreviousFiveWeeksStatusByUserId(int userId) throws ParseException {
		MultiValueMap<String, List<Date>> previousFiveWeeksMap = getPreviousFiveWeeks();
		Map<String,String> previousFiveWeeksStatus = new HashMap<String,String>();
		Iterator<String> iterator = previousFiveWeeksMap.keySet().iterator();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

		while(iterator.hasNext()){
			String weekNumber = (String)iterator.next();
			List<String> weekDatesList = (List<String>) previousFiveWeeksMap.get(weekNumber);
			String weekIdentifierDate = weekDatesList.get(6);
			List<String> weekStatus = dao.findUserSubmittedTimeLogStatusByUserIdAndWeekIdentifierDate(userId, weekIdentifierDate);
			if(!weekStatus.isEmpty()) {
				previousFiveWeeksStatus.put(weekNumber, weekStatus.get(0));
			} else {
				previousFiveWeeksStatus.put(weekNumber, "Open");
			}
		}
		return previousFiveWeeksStatus;
	}

	@Override
	public Map<String, String> getCurrentWeekAndUpcomingFourWeeksStatusByUserId(int userId) throws ParseException {
		MultiValueMap<String, List<Date>> currentWeekAndUpcomingFourWeeksMap = getCurrentWeekAndUpcomingFourWeeks();
		Map<String,String> currentWeekAndUpcomingFourWeeksStatus = new HashMap<String,String>();
		Iterator<String> iterator = currentWeekAndUpcomingFourWeeksMap.keySet().iterator();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

		while(iterator.hasNext()){
			String weekNumber = (String)iterator.next();
			List<String> weekDatesList = (List<String>) currentWeekAndUpcomingFourWeeksMap.get(weekNumber);
			String weekIdentifierDate = weekDatesList.get(6);
			List<String> weekStatus = dao.findUserSubmittedTimeLogStatusByUserIdAndWeekIdentifierDate(userId, weekIdentifierDate);
			if(!weekStatus.isEmpty()) {
				currentWeekAndUpcomingFourWeeksStatus.put(weekNumber, weekStatus.get(0));
			} else {
				currentWeekAndUpcomingFourWeeksStatus.put(weekNumber, "Open");
			}
		}
		return currentWeekAndUpcomingFourWeeksStatus;
	}

	@Override
	public List<UserSubmittedTimeLog> findAllUserSubmittedTimeLogsByStatusesAsSubmittedAndResubmittedAndApprovedAndUserRole(String userRole) {
		return dao.findAllUserSubmittedTimeLogsByStatusesAsSubmittedAndResubmittedAndUserRole(userRole);
	}

	@Override
	public List<String> getSelectedWeekDates(String logDate) throws ParseException {
		List<String> weekDatesList = new ArrayList<>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		Date _logDate = dateFormat.parse(logDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(_logDate);
		for(int i = 6; i > 0; i--) {
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - i);
			Date date = cal.getTime();
			weekDatesList.add(dateFormat.format(date));
			cal.setTime(_logDate);
		}
		weekDatesList.add(logDate);

		return weekDatesList;
	}

	@Override
	public boolean updateUserSubmittedTimeLogsStatus(String userId, String logDate, String status) {
		List<UserSubmittedTimeLog> userSubmittedTimeLogsListByUserIdAndLogDate = dao.findAllUserSubmittedTimeLogsByUserIdAndLogDate(Integer.parseInt(userId), logDate);
		if(!userSubmittedTimeLogsListByUserIdAndLogDate.isEmpty()) {
			for(UserSubmittedTimeLog userSubmittedTimeLog : userSubmittedTimeLogsListByUserIdAndLogDate) {
				userSubmittedTimeLog.setStatus(status);
			}
			return true;
		}
		return false;
	}

}
