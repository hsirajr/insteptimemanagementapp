package com.instep.tma.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.MultiValueMap;

import com.instep.tma.dto.UserSubmittedLogsListDTO;
import com.instep.tma.model.UserSubmittedTimeLog;

public interface TimeEntryService {
	
	MultiValueMap<String, List<Date>> getPreviousFiveWeeks() throws ParseException;
	
	MultiValueMap<String, List<Date>> getCurrentWeekAndUpcomingFourWeeks() throws ParseException;

	Map<String, String> getPreviousFiveWeeksStatusByUserId(int loggedInUser) throws ParseException;
	
	Map<String, String> getCurrentWeekAndUpcomingFourWeeksStatusByUserId(int loggedInUser) throws ParseException;
	
	boolean saveUserSubmittedLog(UserSubmittedLogsListDTO userSubmittedTimeLogsListDTO);

	UserSubmittedTimeLog findById(Integer id);

	List<UserSubmittedTimeLog> findAllUserSubmittedTimeLogsByUserIdAndLogDate(int userId, String logDate);

	UserSubmittedTimeLog findAllUserSubmittedTimeLogsByUserIdAndTaskIdAndLogDate(int userId, int taskId,	String logDate);

	List<UserSubmittedTimeLog> findAllUserSubmittedTimeLogsByStatusesAsSubmittedAndResubmittedAndApprovedAndUserRole(String userRole);

	List<String> getSelectedWeekDates(String logDate) throws ParseException;

	boolean updateUserSubmittedTimeLogsStatus(String userId, String logDate, String status);
	
}
