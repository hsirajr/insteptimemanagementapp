package com.instep.tma.audit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.envers.RevisionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.instep.tma.service.UserService;

@Component
public class RevListener implements RevisionListener {

	@Autowired
	UserService userService;
	
	@Override
	public void newRevision(Object revisionEntity) {
		int loggedInUserId = (getSession().getAttribute("loggedInUser") != null) ? (Integer) getSession().getAttribute("loggedInUser") : -1;
		RevInfo revEntity = (RevInfo) revisionEntity;
		revEntity.setTransactorId(loggedInUserId);
	}

	public HttpSession getSession() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(true); 
	}

	public HttpServletRequest getHttpServletRequest() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest(); 
	}
}
