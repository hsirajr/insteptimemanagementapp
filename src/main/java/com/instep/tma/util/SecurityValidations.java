package com.instep.tma.util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class SecurityValidations {
	public String performMD5Hash(String string) throws Exception {
		MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(string.getBytes(),0,string.length());
	    String hashedString = new BigInteger(1,md.digest()).toString(16);
	    
	    return hashedString;
	}
}
