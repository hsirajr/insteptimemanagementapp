$(document).ready(function() {
	$('#password , #confirmPassword').keyup(function(){
		if($('#password').val().length > 0 && $('#password').val() == $('#confirmPassword').val()){
			$('#password-validation-result').html(checkStrength($('#password').val()));
			$('#password-validation-result').css({'display' : 'block'});
			$('#signUpButton').removeAttr('disabled');
		}else{
			$('#password-validation-result').css({'display' : 'none'});
			$('#signUpButton').attr('disabled','disabled');
		}
	});

	function checkStrength(password) {
		var strength = 0;
		if(password.length < 8) {
			$("#password-validation-result").removeClass();
			$("#password-validation-result").addClass("alert alert-danger");
			return "At least 8 characters required.";
		}
		if(password.length > 9) strength += 1;
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1;
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1;
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
		if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
		if(strength < 2) {
			$("#password-validation-result").removeClass();
			$("#password-validation-result").addClass("alert alert-warning");
			return "Password strength: Weak";
		} else if (strength == 2) {
			$("#password-validation-result").removeClass();
			$("#password-validation-result").addClass("alert alert-info");
			return "Password strength: Good";
		} else {
			$("#password-validation-result").removeClass();
			$("#password-validation-result").addClass("alert alert-success");
			return "Password strength: Strong";
		}
	}
});