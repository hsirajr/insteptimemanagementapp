$(document).ready(function() {

	setFieldsForReedittingTimesheet();
	$('#submitButton').click(function(){
		$('[id*=enteredHours]').each(function() {
		if ($(this).is(":invalid")) {
		    alert("Time-sheet cannot be submitted due to highlighted Invalid Hours.");
		    e.preventDefault();
		}
		});
		$('[id*=ptoHours]').each(function() {
			if ($(this).is(":invalid")) {
			    alert("Time-sheet cannot be submitted due to highlighted Invalid Hours.");
			    e.preventDefault();
			}
			});
		setHiddenFieldsValues();
		submitForm();
	});
	$('#approveTimesheetButton').click(function(){
		$('[id*=enteredHours]').each(function() {
			if ($(this).is(":invalid")) {
			    alert("Time-sheet cannot be submitted due to highlighted Invalid Hours.");
			    e.preventDefault();
			}
		});
		$('[id*=ptoHours]').each(function() {
			if ($(this).is(":invalid")) {
			    alert("Time-sheet cannot be submitted due to highlighted Invalid Hours.");
			    e.preventDefault();
			}
		});
		
		sendStatusAsApproved();
	});
	$('#requestToResubmitTimesheetButton').click(function(){
		$('[id*=enteredHours]').each(function() {
			if ($(this).is(":invalid")) {
			    alert("Time-sheet cannot be submitted due to highlighted Invalid Hours.");
			    e.preventDefault();
			}
		});
		$('[id*=ptoHours]').each(function() {
			if ($(this).is(":invalid")) {
			    alert("Time-sheet cannot be submitted due to highlighted Invalid Hours.");
			    e.preventDefault();
			}
		});
		sendStatusAsReopened();
	});
	
	var $tblrows = $("#timeEntryTable tbody tr");
    $tblrows.each(function (index) {
	    var $tblrow = $(this);
	    $tblrow.find("[id$=-enteredHours-saturday] , [id$=-enteredHours-sunday] , [id$=-enteredHours-monday] , [id$=-enteredHours-tuesday] , [id$=-enteredHours-wednesday] , [id$=-enteredHours-thursday] , [id$=-enteredHours-friday] ").on('change', function () {
	    var staturdayHours = $tblrow.find("[id$=-enteredHours-saturday]").val().length == 0 ? 0.0 : $tblrow.find("[id$=-enteredHours-saturday]").val();
	    var sundayHours = $tblrow.find("[id$=-enteredHours-sunday]").val().length == 0 ? 0.0 :  $tblrow.find("[id$=-enteredHours-sunday]").val();
	    var mondayHours = $tblrow.find("[id$=-enteredHours-monday]").val().length == 0 ? 0.0 :  $tblrow.find("[id$=-enteredHours-monday]").val();
	    var tuesdayHours = $tblrow.find("[id$=-enteredHours-tuesday]").val().length == 0 ? 0.0 :  $tblrow.find("[id$=-enteredHours-tuesday]").val();
	    var wednesdayHours = $tblrow.find("[id$=-enteredHours-wednesday]").val().length == 0 ? 0.0 :  $tblrow.find("[id$=-enteredHours-wednesday]").val();
	    var thursdayHours = $tblrow.find("[id$=-enteredHours-thursday]").val().length == 0 ? 0.0 :  $tblrow.find("[id$=-enteredHours-thursday]").val();
	    var fridayHours = $tblrow.find("[id$=-enteredHours-friday]").val().length == 0 ? 0.0 :  $tblrow.find("[id$=-enteredHours-friday]").val();
	    var subTotal = parseFloat(staturdayHours) + parseFloat(sundayHours) + parseFloat(mondayHours) + parseFloat(tuesdayHours) + parseFloat(wednesdayHours) + parseFloat(thursdayHours) + parseFloat(fridayHours);
	    $tblrow.find("[id$=-subTotal]").val(subTotal.toFixed(2));
	    var grandTotal = 0;
        $("[id$=-subTotal]").each(function () {
            var stval = parseFloat($(this).val());
            grandTotal += isNaN(stval) ? 0 : stval;
        });
        $('.grdtot').val(grandTotal.toFixed(2));
    });
});

$tblrows.each(function (index) {
    var $tblrow = $(this);
    var staturdayHours = $tblrow.find("[id$=-enteredHours-saturday]").val();
    //console.log("staturdayHours = " + staturdayHours);
    var sundayHours = $tblrow.find("[id$=-enteredHours-sunday]").val();
    var mondayHours = $tblrow.find("[id$=-enteredHours-monday]").val();
    var tuesdayHours = $tblrow.find("[id$=-enteredHours-tuesday]").val();
    var wednesdayHours = $tblrow.find("[id$=-enteredHours-wednesday]").val();
    var thursdayHours = $tblrow.find("[id$=-enteredHours-thursday]").val();
    var fridayHours = $tblrow.find("[id$=-enteredHours-friday]").val();
    var subTotal = parseFloat(staturdayHours) + parseFloat(sundayHours) + parseFloat(mondayHours) + parseFloat(tuesdayHours) + parseFloat(wednesdayHours) + parseFloat(thursdayHours) + parseFloat(fridayHours);
    $tblrow.find("[id$=-subTotal]").val(subTotal.toFixed(2));
    var grandTotal = 0;
    $("[id$=-subTotal]").each(function () {
        var stval = parseFloat($(this).val());
        grandTotal += isNaN(stval) ? 0 : stval;
    });
    $('.grdtot').val(grandTotal.toFixed(2));
});   


var ptoGrandTotal = 0;
$('[id*=ptoHours]').on('change', function(){
	var ptoGrandTotal = 0;
	$('[id*=ptoHours]').each(function () {
        var stval = parseFloat($(this).val());
        ptoGrandTotal += isNaN(stval) ? 0 : stval;
    });
    $('#totalPtoHours').val(ptoGrandTotal.toFixed(2));
});

var submittedptoCSV = $("#totalPtoHoursPerWeek").val();
var slpittedptoCSV = submittedptoCSV.split(",");
var ptoGrandTotal = 0;
$('[id*=ptoHours]').each(function (index) {
	$(this).val(slpittedptoCSV[index]);
	ptoGrandTotal += parseFloat($(this).val());
});
$('#totalPtoHours').val(ptoGrandTotal.toFixed(2));

/* Disabling enteredHours and ptoHours fields */

console.log("timeSheetStatus : " + timeSheetStatus + " ,  isAdmin : " + isAdmin );
debugger;
if((timeSheetStatus == "") || (timeSheetStatus == "Reopened") || (isAdmin == "true") ){
	$('[id*=enteredHours]').each(function() {
		$(this).removeAttr("disabled");
	});
	$('[id*=ptoHours]').each(function() {
		$(this).removeAttr("disabled");
	});
} else{
	$('[id*=enteredHours]').each(function() {
		$(this).attr("disabled","disabled");
	});
	$('[id*=ptoHours]').each(function() {
		$(this).attr("disabled","disabled");
	});
	$("#submitButton").hide();
	$("#submitButtonContainer").css({'left' : '10%'});
}

/*if((timeSheetStatus != "") || (timeSheetStatus != "Reopened") && isAdmin != "true" ){
	$('[id*=enteredHours]').each(function() {
		$(this).attr("disabled","disabled");
		$("#submitButton").hide();
	});
}*/

});

function setFieldsForReedittingTimesheet() {
	var taskId = "";
	var taskIdsArray = [];
	var hoursPerDay = "";
	$('[id*=enteredHours]').each(function() {
		taskId = $(this).attr("id").split("-")[0];
		if(taskIdsArray.indexOf(taskId) <= -1) {
			taskIdsArray.push(taskId);
		}
	});

	//console.log("taskIdsArray: " + taskIdsArray);

	for (var i = 0; i < taskIdsArray.length; i++) {
		hoursPerDay = $("#" + taskIdsArray[i] + "-hoursPerDayPerWeek").val().split(",");
		//console.log("hoursPerDay: " + hoursPerDay);
		if(hoursPerDay.length > 1) {
			$("#" + taskIdsArray[i] + "-enteredHours-saturday").val(hoursPerDay[0]);
			$("#" + taskIdsArray[i] + "-enteredHours-sunday").val(hoursPerDay[1]);
			$("#" + taskIdsArray[i] + "-enteredHours-monday").val(hoursPerDay[2]);
			$("#" + taskIdsArray[i] + "-enteredHours-tuesday").val(hoursPerDay[3]);
			$("#" + taskIdsArray[i] + "-enteredHours-wednesday").val(hoursPerDay[4]);
			$("#" + taskIdsArray[i] + "-enteredHours-thursday").val(hoursPerDay[5]);
			$("#" + taskIdsArray[i] + "-enteredHours-friday").val(hoursPerDay[6]);
		} else {
			$("#" + taskIdsArray[i] + "-enteredHours-saturday").val("0.0");
			$("#" + taskIdsArray[i] + "-enteredHours-sunday").val("0.0");
			$("#" + taskIdsArray[i] + "-enteredHours-monday").val("0.0");
			$("#" + taskIdsArray[i] + "-enteredHours-tuesday").val("0.0");
			$("#" + taskIdsArray[i] + "-enteredHours-wednesday").val("0.0");
			$("#" + taskIdsArray[i] + "-enteredHours-thursday").val("0.0");
			$("#" + taskIdsArray[i] + "-enteredHours-friday").val("0.0");
		}
	}
}

function setHiddenFieldsValues() {
	//debugger;
	var submittedHoursPerDayCSV = "";
	var submittedptoHoursCSV = "";
	var totalHoursPerWeek = 0.0;
	var totalHoursPerWeekArray = [];
	var taskId = "";
	var taskIdsArray = [];

	$('[id*=enteredHours]').each(function() {
		taskId = $(this).attr("id").split("-")[0];
		if(taskIdsArray.indexOf(taskId) <= -1) {
			taskIdsArray.push(taskId);
		}
		if($(this).val().length > 0){
			submittedHoursPerDayCSV += $(this).val() + ",";
		} else{
			submittedHoursPerDayCSV += $(this).val() + "0.0,";	
		}
		//console.log("submittedHoursPerDayCSV = " + submittedHoursPerDayCSV);
		//totalHoursPerWeek += parseFloat($(this).val());
	});
	var hoursPerDayPerWeek = splitCSVPerWeek(submittedHoursPerDayCSV, 7); //PER WEEK
	//console.log("hoursPerDayPerWeek = " + hoursPerDayPerWeek);
	for (var i = 0; i < hoursPerDayPerWeek.length; i++) {
		totalHoursPerWeek = 0.0;
		var temp = [];
		//hoursPerDayPerWeek[i] = hoursPerDayPerWeek[i].slice(0,-1);
		hoursPerDayPerWeek[i] = hoursPerDayPerWeek[i];
		//console.log("hoursPerDayPerWeek[i]: " + hoursPerDayPerWeek[i]);
		temp = hoursPerDayPerWeek[i].split(",");
		for (var j = 0; j < temp.length; j++) {
			//console.log("temp[" + j + "]: " + temp[j]);
			totalHoursPerWeek += parseFloat(temp[j]);
		}
		totalHoursPerWeekArray.push(totalHoursPerWeek);
		//console.log("totalHoursPerWeekArray: " + totalHoursPerWeekArray);
	}

	for (var i = 0; i < taskIdsArray.length; i++) {
		$("#" + taskIdsArray[i] + "-hoursPerDayPerWeek").val(hoursPerDayPerWeek[i]);
		//$("#" + taskIdsArray[i] + "-totalHoursPerWeek").val(totalHoursPerWeekArray[i].toFixed(1));
		$("#" + taskIdsArray[i] + "-totalHoursPerWeek").val(totalHoursPerWeekArray[i]);
		$("#" + taskIdsArray[i] + "-date").val($("#weekIdentifierDate").text());
	}
	
	$('[id*=ptoHours]').each(function() {
		
		if($(this).val().length > 0){
			submittedptoHoursCSV += $(this).val() + ",";
		} else{
			submittedptoHoursCSV += $(this).val() + "0.0,";	
		}
		//console.log("submittedptoHoursCSV = " + submittedptoHoursCSV);
		$("#totalPtoHoursPerWeek").val(submittedptoHoursCSV);
		//alert(submittedptoHoursCSV);
	});

}

function submitForm(){
	/*if($("#isAdminViewingTimesheetHiddenField").val() == true) {
		//console.log("AdminViewingTimesheet");
		sendStatusAsReopened();
	}*/
	$("#timeEntryForm").submit();
}

function splitCSVPerWeek(txt, splitLength) {
	//debugger;
	 var a = txt.split(','), b = [];
	  while(a.length) b.push(a.splice(0,splitLength).join(','));
	  return b;
}

function sendStatusAsApproved() {
	var userId = $('#userTimesheetBelongsTo').val();
	var logDate = $('#weekIdentifierDate').text();
	
	//console.log("userId: " + userId + ", logDate: " + logDate);
	var jsonString = JSON.stringify({ 
		"userId": userId,
	    "logDate": logDate,
	    "status": "Approved"
	    });
	
	$.ajax({ 
	    url:"../InstepTimeManagementApp/changeTimesheetStatus",
	    type:"POST", 
	    contentType: "application/json; charset=utf-8",
	    data: jsonString, 
	    success: function(data){
	    	$('#requestToResubmitSuccessfullyLabel').hide();
	    	$('#approvedSuccessfullyLabel').show();
	    },
	    error: function(error) {
	    	console.dir(error);
	    }
	});
}

function sendStatusAsReopened() {
	var userId = $('#userTimesheetBelongsTo').val();
	var logDate = $('#weekIdentifierDate').text();
	
	var jsonString = JSON.stringify({ 
		"userId": userId,
	    "logDate": logDate,
	    "status": "Reopened"
	    });
	
	$.ajax({ 
	    url:"../InstepTimeManagementApp/changeTimesheetStatus",
	    type:"POST", 
	    contentType: "application/json; charset=utf-8",
	    data: jsonString, 
	    success: function(data){
	    	$('#approvedSuccessfullyLabel').hide();
	    	$('#requestToResubmitSuccessfullyLabel').show();
	    	
	    },
	    error: function(error) {
	    	console.dir(error);
	    }
	});
}