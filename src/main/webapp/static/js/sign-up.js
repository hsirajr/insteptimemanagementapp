$(document).ready(function() {
	$('#signUpButton').click(function(){
		postSignUpCredentials();
		$('#password-validation-result').css({'display' : 'none'});
	});
	
	
});

function postSignUpCredentials() {
	console.log("signing up ");
	var username = $('#username').val();
	var password = $('#password').val();
	var jsonString = JSON.stringify({ 
		"username": username,
		"password": password
	});

	$.ajax({ 
		url:"../InstepTimeManagementApp/sign-up",
		type:"POST", 
		contentType: "application/json; charset=utf-8",
		data: jsonString, 
		success: function(data){
			$("#userRegistrationSuccessfulLabel").css({'display':'block'});
			$("#usernameNotRegisteredLabel").css({'display':'none'});
		},
		error: function(data){
			if(data.status == "404") {
				$("#userRegistrationSuccessfulLabel").css({'display':'none'});
				$("#usernameNotRegisteredLabel").css({'display':'block'});
				
			}
			if(data.status == "200") {
				$("#userRegistrationSuccessfulLabel").css({'display':'block'});
				$("#usernameNotRegisteredLabel").css({'display':'none'});
			}
		}
	});
}