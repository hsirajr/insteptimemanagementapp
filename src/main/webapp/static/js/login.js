$(document).ready(function() {
	$('#signInButton').click(function(){
		postLoginCredentials();
	});
});

function postLoginCredentials() {
	console.log("logging in ");
	var username = $('#username').val();
	var password = $('#password').val();
	
	var jsonString = JSON.stringify({ 
		"username": username,
	    "password": password
	    });
	
	$.ajax({ 
	    url:"../InstepTimeManagementApp/login",
	    type:"POST", 
	    contentType: "application/json; charset=utf-8",
	    data: jsonString, 
	    async: false,   
	    cache: false,     
	     processData:false, 
	     success: function(data, textStatus, xhr){
	        console.log("data" + data);
	        window.location = xhr.getResponseHeader("Location");
	    }
	});
}