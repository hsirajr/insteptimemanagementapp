$(document).ready(function() {
	
	toggleSubmitButtonDisable();
	getModulesForSelectedProject();
	$("#leadsListDropDown").change(toggleSubmitButtonDisable);
	$("#clientsListDropDown").change(toggleSubmitButtonDisable);
	$("#projectsListDropDown").change(getModulesForSelectedProject);
	$("#modulesListDropDown").change(getTasksForSelectedModule);
	$("#tasksListDropDown").change(getAllocatedAndNonAllocatedUsersForSelectedTask);
	$('#allocateResourcesFormSubmitButton').click(function(){
		setHiddenFieldsAndSubmitForm();
	});
});


$(function () { function moveItems(origin, dest) {
    $(origin).find(':selected').appendTo(dest);
}
 
function moveAllItems(origin, dest) {
    $(origin).children().appendTo(dest);
}
 
$('#left').click(function () {
    moveItems('#allocatedUsersListDropDown', '#nonAllocatedUsersListDropDown');
});
 
$('#right').on('click', function () {
    moveItems('#nonAllocatedUsersListDropDown', '#allocatedUsersListDropDown');
});
 
$('#leftall').on('click', function () {
    moveAllItems('#allocatedUsersListDropDown', '#nonAllocatedUsersListDropDown');
});
 
$('#rightall').on('click', function () {
    moveAllItems('#nonAllocatedUsersListDropDown', '#allocatedUsersListDropDown');
});
});

$('#left').click(function () {
    moveItems('#allocatedUsersListDropDown', '#nonAllocatedUsersListDropDown');
});
 
$('#right').on('click', function () {
    moveItems('#nonAllocatedUsersListDropDown', '#allocatedUsersListDropDown');
});
 
$('#leftall').on('click', function () {
    moveAllItems('#allocatedUsersListDropDown', '#nonAllocatedUsersListDropDown');
});
 
$('#rightall').on('click', function () {
    moveAllItems('#nonAllocatedUsersListDropDown', '#allocatedUsersListDropDown');
});

function getModulesForSelectedProject() {
	toggleSubmitButtonDisable();
	var moduleDropdownDefaultSelectOption = '<option value="-1">Choose Module</option>';
	var noModulesAllocatedToProject = '<option value="-1">No modules allocated to the project yet.</option>';
	if($("#projectsListDropDown").val() == -1){
		$("#modulesListDropDown").empty().append("<option value=\"-1\">Choose Project First</option>");
		$("#tasksListDropDown").empty().append("<option value=\"-1\">Choose Module First</option>");
		$("#allocatedUsersListDropDown").empty();
		$("#nonAllocatedUsersListDropDown").empty();
	} else {
		$.getJSON("../InstepTimeManagementApp/getModulesForSelectedProject",{projectId: $("#projectsListDropDown").val()}, function(j){
			console.log(j);
			var options = '';
			options += moduleDropdownDefaultSelectOption;
			for (var i = 0; i < j.length; i++) {
				options += '<option value="' + j[i].id + '">' + j[i].name + '</option>';
			}
			$("#modulesListDropDown").html(options);
/*			debugger;
			$("#modulesListDropDown  option").each( function() {
				if (this.value == moduleVal) {
					$(this).attr("selected", "selected");
				}
			});*/
		}).error(function() { $("#modulesListDropDown").html(noModulesAllocatedToProject);  });
	}
}

function getTasksForSelectedModule() {
	toggleSubmitButtonDisable();
	var taskDropdownDefaultSelectOption = '<option value="-1">Choose Task</option>';
	var noTasksAllocatedToModule = '<option value="-1">No tasks allocated to the module yet</option>';
	if($("#modulesListDropDown").val() == -1){
		$("#tasksListDropDown").empty().append("<option value=\"-1\">Choose Module First</option>");
		$("#allocatedUsersListDropDown").empty();
		$("#nonAllocatedUsersListDropDown").empty();
	} else{
		$.getJSON("../InstepTimeManagementApp/getTasksForSelectedModule",{moduleId: $("#modulesListDropDown").val()}, function(j){
			var options = '';
			options += taskDropdownDefaultSelectOption;
			for (var i = 0; i < j.length; i++) {
				options += '<option value="' + j[i].id + '" data-text="'+ j[i].name + '">' + j[i].name + '</option>';
			}
			$("#tasksListDropDown").html(options);
		}).error(function() { $("#tasksListDropDown").html(noTasksAllocatedToModule);  }); 
	}
}

function getAllocatedAndNonAllocatedUsersForSelectedTask() {
	toggleSubmitButtonDisable();
	if($("#tasksListDropDown").val() == -1){
		$("#allocatedUsersListDropDown").empty();
		$("#nonAllocatedUsersListDropDown").empty();
	} else{		
		$.getJSON("../InstepTimeManagementApp/getAllocatedUsersForSelectedTask",{taskId: $("#tasksListDropDown").val()}, function(j){
			var options = '';
			//options += taskDropdownDefaultSelectOption;
			for (var i = 0; i < j.length; i++) {
				options += '<option value="' + j[i].id + '">' + j[i].firstName + ' ' + j[i].lastName  + '</option>';
			}
			$("#allocatedUsersListDropDown").html(options);
		}).error(function() { $("#allocatedUsersListDropDown").empty(); }); 
		
		$.getJSON("../InstepTimeManagementApp/getNonAllocatedUsersForSelectedTask",{taskId: $("#tasksListDropDown").val()}, function(j){
			var options = '';
			//options += taskDropdownDefaultSelectOption;
			for (var i = 0; i < j.length; i++) {
				options += '<option value="' + j[i].id + '">' + j[i].firstName + ' ' + j[i].lastName  + '</option>';
			}
			$("#nonAllocatedUsersListDropDown").html(options);
		}).error(function() { $("#nonAllocatedUsersListDropDown").empty(); });
	}
	
}

function setHiddenFieldsAndSubmitForm() {
	$("#allocatedUsersListDropDown  option").each( function() {
		$(this).removeAttr("selected");
	});
	
	$("#nonAllocatedUsersListDropDown  option").each( function() {
		$(this).removeAttr("selected");
	});

	var allocatedUsersArray = [];
	var allocatedUsersToBeUnallocatedArray = $("#allocatedUsersListDropDown").val();
	var nonAllocatedUsersArray = $("#nonAllocatedUsersListDropDown").val();
	var allocatedAndNonAllocatedUsersArray = [];

	$("#allocatedUsersListDropDown option").each(function() {
		allocatedUsersArray.push($(this).val());
	});

	console.log("allocatedUsersArray " + allocatedUsersArray);
	console.log("allocatedUsersToBeUnallocatedArray " + allocatedUsersToBeUnallocatedArray);
	console.log("nonAllocatedUsersArray " + nonAllocatedUsersArray);
	
	if(allocatedUsersToBeUnallocatedArray != null) {
		allocatedUsersArray = allocatedUsersArray.filter(function(val) {
			return allocatedUsersToBeUnallocatedArray.indexOf(val) == -1;
		});
	}
	
	if(nonAllocatedUsersArray == null)	
		$.merge( allocatedAndNonAllocatedUsersArray, allocatedUsersArray );
	else 
		$.merge($.merge( allocatedAndNonAllocatedUsersArray, allocatedUsersArray ), nonAllocatedUsersArray );
	
	var jsonString = JSON.stringify({ 
		"taskId": $("#tasksListDropDown").val(),
		"usersList": allocatedAndNonAllocatedUsersArray
	});

	$.ajax({ 
		url:"../InstepTimeManagementApp/allocate-resource",
		type:"POST", 
		contentType: "application/json; charset=utf-8",
		data: jsonString, 
		success: function(data){
			$('#resourceAllocationSuccessfulLabel').show();
		},
		error: function(error) {
			$('#resourceAllocationFailedLabel').show();
		}
	});
}
function toggleSubmitButtonDisable(){
	debugger;
	if(($("#projectsListDropDown").val() == "-1") || ($("#modulesListDropDown").val() == "-1") || ($("#clientsListDropDown").val() == "-1") || ($("#tasksListDropDown").val() == "-1")  ){
		$("#submitButton").attr("disabled", "disabled");
		$("#allocateResourcesFormSubmitButton").attr("disabled", "disabled");
		
	} else{
		$("#submitButton").removeAttr("disabled");
		$("#allocateResourcesFormSubmitButton").removeAttr("disabled");
	}
}
function resetPassword(){
	if(confirm('Are you sure you want to reset password for this user ?')){
		
		var jsonString = JSON.stringify({ 
			"userId": $("#userId").val()
		});
		console.log("userId" + $("#userId").val());
		$.ajax({ 
			url:"../InstepTimeManagementApp/resetPassword",
			type:"POST", 
			contentType: "application/json; charset=utf-8",
			data: jsonString,
			success: function(data){
				$('#resetPasswordSuccessfulLabel').show();
			},
			error: function(error) {
				$('#resetPasswordFailedLabel').show();
			}
		});
	}
	
}