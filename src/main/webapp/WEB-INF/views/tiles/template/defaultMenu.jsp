<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<nav class="navbar navbar-inverse sidebar" role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header" style="height: 10%;">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#" style="font-size:1.3em;">INSTEP Time App</a>
			<a class="navbar-brand" href="#" style="font-size:0.7em;"> Welcome : ${loggedInUserName}</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active"><a href="${pageContext.request.contextPath}/home">Home<span
						style="font-size: 16px;"
						class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li>
					<li class="divider"></li>
				<sec:authorize access="hasAnyRole('SUPER_ADMIN', 'PROJECT_ADMIN')">	
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Admin Section
							<span class="caret"></span>
							<span style="font-size: 16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span>
						</a>
						<ul class="dropdown-menu forAnimate" role="menu">
						<sec:authorize access="hasAnyRole('SUPER_ADMIN')">	
							<li class="dropdown dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Users
								</a>
								<ul class="dropdown-menu forAnimate" role="menu">
									<li>
										<a href="${pageContext.request.contextPath}/add-user">
										Add User</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/view-users">
											View All Users</a>
									</li>
								</ul>
							</li>
							</sec:authorize>
							<li class="dropdown dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Clients
								</a>
								<ul class="dropdown-menu forAnimate" role="menu">
									<li>
										<a href="${pageContext.request.contextPath}/add-client">
										Add Client</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/view-clients">
											View All Clients</a>
									</li>
								</ul>
							</li>
							<li class="dropdown dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Projects
								</a>
								<ul class="dropdown-menu forAnimate" role="menu">
									<li>
										<a href="${pageContext.request.contextPath}/add-project">
										Add Project</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/view-projects">
											View All Projects</a>
									</li>
								</ul>
							</li>
							<li class="dropdown dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Modules
								</a>
								<ul class="dropdown-menu forAnimate" role="menu">
									<li>
										<a href="${pageContext.request.contextPath}/add-module">
										Add Module</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/view-modules">
											View All Modules</a>
									</li>
								</ul>
							</li>
							<li class="dropdown dropdown-submenu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									Tasks
								</a>
								<ul class="dropdown-menu forAnimate" role="menu">
									<li>
										<a href="${pageContext.request.contextPath}/add-task">
										Add Task</a>
									</li>
									<li>
										<a href="${pageContext.request.contextPath}/view-tasks">
											View All Tasks</a>
									</li>
								</ul>
							</li>
							
							
							<%-- <li><a href="${pageContext.request.contextPath}/add-client">Add
									Client</a></li>
							<li><a href="${pageContext.request.contextPath}/add-project">Add
									Project</a></li>
							<li><a href="${pageContext.request.contextPath}/add-module">Add
									Module</a></li>
							<li><a href="${pageContext.request.contextPath}/add-task">Add
									Task</a></li> --%>
							<li><a href="${pageContext.request.contextPath}/allocate-resource">Allocate
									Resources</a></li>
						</ul></li>
						</sec:authorize>					
						<li><a href="${pageContext.request.contextPath}/view-timesheets">Time
								Sheets<span style="font-size: 16px;"
								class="pull-right hidden-xs showopacity glyphicon glyphicon-list"></span>
						</a></li>
					<sec:authorize access="hasAnyRole('LEAD')">	
					<li><a
						href="${pageContext.request.contextPath}/team-timesheets">Approve
							Time Sheets<span style="font-size: 16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-ok"></span>
					</a></li>
				</sec:authorize>
				<li class="divider"></li>
				<li>
					<a href="${pageContext.request.contextPath}/logout">Logout
					<span style="font-size: 16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-log-out"></span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<div class="main">
	<!-- Content Here -->
</div>