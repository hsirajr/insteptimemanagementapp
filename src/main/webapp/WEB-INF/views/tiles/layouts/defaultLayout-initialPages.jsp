<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><tiles:getAsString name="title" /></title>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/sideMenu.css' />" rel="stylesheet"></link>
<!-- <script type="text/javascript"
	src="//code.jquery.com/jquery-1.10.2.min.js"></script> -->
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<script src="<c:url value='/static/js/bootstrap.min.js' />"></script>
<!-- <script type="text/javascript"
	src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
</head>

<body>
	<%-- <header id="header">
            <tiles:insertAttribute name="header" />
        </header> --%>

	<section id="site-content">
		<tiles:insertAttribute name="body" />
	</section>

</body>
</html>