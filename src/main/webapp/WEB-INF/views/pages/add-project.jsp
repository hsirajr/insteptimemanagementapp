<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<c:choose>
	<c:when test="${edit}">
		<title>Instep Time App - Edit Project</title>
	</c:when>
	<c:otherwise>
		<title>Instep Time App - Add Project</title>
	</c:otherwise>
</c:choose>
<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<script src="<c:url value='/static/js/admin-management.js' />"></script>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
<!-- <link rel="stylesheet" type="text/css"
	href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" /> -->
</head>

<body>
	<div class="container">
		<form:form id="addProjectForm" method="POST" modelAttribute="project" class="form-horizontal">
		<div class="row">
			<c:choose>
				<c:when test="${edit}">
					<div class="form-group col-md-12">
				<h3 class="col-md-3 ">Update Project </h3>
				
			</div>
				</c:when>
				<c:otherwise>
					<div class="form-group col-md-12">
				<h3 class="col-md-3 ">Add Project </h3>
				
			</div>
				</c:otherwise>
			</c:choose>
		</div>
			<c:if test="${error != null}">
				<div class="alert alert-danger">
					<p>${error}</p>
				</div>
			</c:if>
			<c:if test="${success != null}">
				<div class="alert alert-success">
					<p>${success}</p>
				</div>
			</c:if>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="name">Project
						Title </label>
					<div class="col-md-3">
						<form:input type="text" path="name" id="name"
							class="form-control input-sm" required="required" />
						<div class="has-error">
							<form:errors path="name" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="description">Description
					</label>
					<div class="col-md-3">
						<form:input type="text" path="description" id="description"
							class="form-control input-sm" required="required" />
						<div class="has-error">
							<form:errors path="description" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<c:choose>
				<c:when test="${edit}">
					<div class="row">
						<div class="form-group col-md-12">
							<label class="col-md-2 " for="client">Client
							</label>
							<div class="col-md-3">
								<form:select id="clientsListDropDown" path="client" class="form-control input-sm">
								<form:option value="-1">Choose Client</form:option>
									<form:options itemValue="id" itemLabel="name"
										items="${clientsList}" />
								</form:select>
								<div class="has-error">
									<form:errors path="client" class="help-inline" />
								</div>
							</div>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="col-md-2 " for="client">Client
							</label>
							<div class="col-md-3">
								<select id="clientsListDropDown" name="client" class="form-control input-sm">
									<option value="-1">Choose Client</option>
										<c:forEach items="${clientsList}" var="client">
										<option class="form-control input-sm" value="${client.id}"
											${client.id == project.client.id  ? 'selected' : ''}>
											${client.name}</option>
									</c:forEach>
								</select>
								<div class="has-error">
									<form:errors path="client" class="help-inline" />
								</div>
							</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			
			<div class="row">
			<c:choose>
				<c:when test="${edit}">
					<div class="form-group col-md-12">
						<div class="col-md-2  col-md-offset-3" style="position:relative; left:3.5%;">
						<input id="submitButton" type="submit" value="Update" class="btn btn-sm btn-primary"/>
						<a href="${pageContext.request.contextPath}/view-projects"  class="btn btn-sm btn-primary">Cancel</a>
					</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="form-group col-md-12">
						<div class="col-md-2  col-md-offset-3" style="position:relative; left:3.5%;">
							<input id="submitButton" type="submit" value="Create" class="btn btn-sm btn-primary"  />
							<a href="${pageContext.request.contextPath}/view-projects"  class="btn btn-sm btn-primary">Cancel</a>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		</form:form>
	</div>
</body>
</html>