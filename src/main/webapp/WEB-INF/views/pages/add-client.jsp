<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<c:choose>
	<c:when test="${edit}">
		<title>Instep Time App - Edit Client</title>
	</c:when>
	<c:otherwise>
		<title>Instep Time App - Add Client</title>
	</c:otherwise>
</c:choose>
<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/font-awesome.css' />"
	rel="stylesheet"></link>
<!-- <link rel="stylesheet" type="text/css"
	href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" /> -->
</head>

<body>
	<div class="container">
		
		<form:form id="addClientForm" method="POST" modelAttribute="client" class="form-horizontal">
		<div class="row">
			<c:choose>
				<c:when test="${edit}">
					<div class="form-group col-md-12">
				<h3 class="col-md-3 ">Update Client </h3>
			</div>
				</c:when>
				<c:otherwise>
					<div class="form-group col-md-12">
				<h3 class="col-md-3 ">Add Client </h3>
			</div>
				</c:otherwise>
			</c:choose>
		</div>
			<c:if test="${error != null}">
				<div class="alert alert-danger">
					<p>${error}</p>
				</div>
			</c:if>
			<c:if test="${success != null}">
				<div class="alert alert-success">
					<p>${success}</p>
				</div>
			</c:if>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="name">Client
						Title </label>
					<div class="col-md-3">
						<form:input type="text" path="name" id="name"
							class="form-control input-sm" required="required" />
						<div class="has-error">
							<form:errors path="name" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="description">Description
					</label>
					<div class="col-md-3">
						<form:input type="text" path="description" id="description"
							class="form-control input-sm" required="required" />
						<div class="has-error">
							<form:errors path="description" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			<c:choose>
				<c:when test="${edit}">
					<div class="form-group col-md-12">
						<div class="col-md-2  col-md-offset-3" style="position:relative; left:3.5%;">
						<input type="submit" value="Update" class="btn btn-sm btn-primary"/>
						<a href="${pageContext.request.contextPath}/view-clients"  class="btn btn-sm btn-primary">Cancel</a>
					</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="form-group col-md-12">
						<div class="col-md-2  col-md-offset-3" style="position:relative; left:3.5%;">
							<input type="submit" value="Create" class="btn btn-sm btn-primary"  />
							<a href="${pageContext.request.contextPath}/view-clients"  class="btn btn-sm btn-primary">Cancel</a>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		</form:form>
	</div>
</body>
</html>