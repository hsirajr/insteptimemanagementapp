<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<title>Instep Time App - Edit TimeSheet</title>
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<script src="<c:url value='/static/js/time-entry.js' />"></script>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
<script>
var timeSheetStatus = '<c:out value="${userSubmittedTimeLogsList.userSubmittedTimeLogsList[0].status}"/>';
var isAdmin = '<c:out value="${isLoggedInUserAdmin}"/>';
</script>
<style class="cp-pen-styles">
table {
	font-family: sans-serif;
	width: 100%;
	border-spacing: 0;
	border-collapse: separate;
	table-layout: fixed;
	margin-bottom: 50px;
}

table thead tr th {
	text-align:center;
	background: #626E7E;
	color: #d1d5db;
	padding: 0.5em;
	overflow: hidden;
}

table thead tr th:first-child {
	border-radius: 3px 0 0 0;
}

table thead tr th:last-child {
	border-radius: 0 3px 0 0;
}

table thead tr th .day {
	/* display: block; */
	font-size: 0.8em;
	border-radius: 50%;
	width: 30px;
	height: 30px;
	margin: 0 auto 5px;
	padding: 5px;
	line-height: 1.8;
}

table thead tr th .day.active {
	background: #d1d5db;
	color: #626E7E;
}

table thead tr th .short {
	display: none;
}

table thead tr th i {
	vertical-align: middle;
	font-size: 2em;
}

table tbody tr {
	background: #d1d5db;
}

table tbody tr:nth-child(odd) {
	background: #c8cdd4;
}

.project-seperator {
	border-top: 1px solid #626E7E;
}

/* table tbody tr:nth-child(3n+0) td {
	border-bottom: 1px solid #626E7E;
} */
table tbody tr td {
	/* text-align: center; */
	vertical-align: middle;
	border-left: 1px solid #626E7E;
	position: relative;
	height: 32px;
	cursor: pointer;
}

table tbody tr td:last-child {
	border-right: 1px solid #626E7E;
}

table tbody tr td.hour {
	font-size: 1em;
	padding: 0;
	color: #626E7E;
	background: #fff;
	border-bottom: 1px solid #626E7E;
	border-collapse: separate;
	min-width: 100px;
	cursor: default;
}

table tbody tr td.hour span {
	display: block;
}

@media ( max-width : 60em) {
	table thead tr th .long {
		display: none;
	}
	table thead tr th .short {
		display: block;
	}
	table tbody tr td.hour span {
		transform: rotate(270deg);
		-webkit-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
	}
}

@media ( max-width : 27em) {
	table thead tr th {
		font-size: 65%;
	}
	table thead tr th .day {
		display: block;
		font-size: 1.2em;
		border-radius: 50%;
		width: 20px;
		height: 20px;
		margin: 0 auto 5px;
		padding: 5px;
	}
	table thead tr th .day.active {
		background: #d1d5db;
		color: #626E7E;
	}
	table tbody tr td.hour {
		font-size: 1.7em;
	}
	table tbody tr td.hour span {
		transform: translateY(16px) rotate(270deg);
		-webkit-transform: translateY(16px) rotate(270deg);
		-moz-transform: translateY(16px) rotate(270deg);
	}
}
:invalid {
    color: red;
}
</style>

</head>

<body>
	<div class="container">
		<div class="row">
			<div class="form-group col-md-12">
				<h3 class="col-md-5 control-label">${userName} 's Time Entry </h3>
			</div>
		</div>
		<div class="row">
			<div style="display:none" id="approvedSuccessfullyLabel" class="alert alert-success" >
				<p>Time-sheet approved successfully.</p>
			</div>
		</div>
		<div class="row">
			<div style="display:none" id="requestToResubmitSuccessfullyLabel" class="alert alert-success" >
				<p>Request for time-sheet resubmit sent successfully.</p>
			</div>
		</div>
		<form:form method="post" id="timeEntryForm"
			action="${pageContext.request.contextPath}/time-entry"
			modelAttribute="${userSubmittedTimeLogsList}">
			<c:if test="${error != null}">
				<div class="alert alert-danger">
					<p>${error}</p>
				</div>
			</c:if>
			<c:if test="${success != null}">
				<div class="alert alert-success">
					<p>${success}</p>
				</div>
			</c:if>
			<c:if test="${isAdminViewingTimesheet == true}">
				<input type="hidden" id="userTimesheetBelongsTo"
					value="<c:out value="${userTimesheetBelongsTo}"/>"/>
				<input type="hidden" id="isAdminViewingTimesheetHiddenField" 
					value="${isAdminViewingTimesheet}">
			</c:if>
			<c:if test="${isAdminViewingTimesheet == false}">
				<input type="hidden" id="userTimesheetBelongsTo"
					value="<c:out value="${loggedInUser}"/>"/>
			</c:if>
			<c:choose>
				<c:when test="${not empty tasksForCurrentUserPerModuleMap}">
					<table id="timeEntryTable">
						<c:if test="${selectedWeekDatesList != null}">
							<thead>
								<tr>
									<th style="width:20%;"><span class="day">${fn:replace(selectedWeekDatesList[0], '[', '')}</span> - <span class="day" id="weekIdentifierDate">${selectedWeekDatesList[6]}</span> </th>
									<th><span class="long">Sat</span></th>
									<th><span class="long">Sun</span></th>
									<th><span class="long">Mon</span></th>
									<th><span class="long">Tue</span></th>
									<th><span class="long">Wed</span></th>
									<th><span class="long">Thu</span></th>
									<th><span class="long">Fri</span></th>
									<th>Total</th>
								</tr>
							</thead>
						</c:if>
						<tbody>
							<c:set var="counter" value="0" />
							<c:forEach items="${tasksForCurrentUserPerModuleMap}" var="task"
								varStatus="outerLoop">
								<tr>
									<td style="padding-left:1%;  font-weight: bold;" class="project-seperator">${task.value[0].module.project.name}</td>
									<td class="project-seperator"></td>
									<td class="project-seperator"></td>
									<td class="project-seperator"></td>
									<td class="project-seperator"></td>
									<td class="project-seperator"></td>
									<td class="project-seperator"></td>
									<td class="project-seperator"></td>
									<td class="project-seperator"></td>
								</tr>
								<tr>
									<td style="padding-left:2%;  font-weight: bold;">${task.value[0].module.name}</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<c:forEach items="${task.value}" var="taskValue"
									varStatus="innerLoop">
									<tr>
										<td style="padding-left:3%;">${taskValue.name}</td>
										<td><input type="number" min="0.0" max="23.75" step="0.25" required="required"
											id="task${taskValue.id}-enteredHours-saturday" value="0.0"
											class="form-control input-sm allowNumericWithDecimal" /></td>
										<td><input type="number" min="0.0" max="23.75" step="0.25" required="required"
											id="task${taskValue.id}-enteredHours-sunday" value="0.0"
											class="form-control input-sm allowNumericWithDecimal" /></td>
										<td><input type="number" min="0.0" max="23.75" step="0.25" required="required"
											id="task${taskValue.id}-enteredHours-monday" value="0.0"
											class="form-control input-sm allowNumericWithDecimal" /></td>
										<td><input type="number" min="0.0" max="23.75" step="0.25" required="required"
											id="task${taskValue.id}-enteredHours-tuesday" value="0.0"
											class="form-control input-sm allowNumericWithDecimal" /></td>
										<td><input type="number" min="0.0" max="23.75" step="0.25" required="required"
											id="task${taskValue.id}-enteredHours-wednesday" value="0.0"
											class="form-control input-sm allowNumericWithDecimal" /></td>
										<td><input type="number" min="0.0" max="23.75" step="0.25" required="required"
											id="task${taskValue.id}-enteredHours-thursday" value="0.0"
											class="form-control input-sm allowNumericWithDecimal" /></td>
										<td><input type="number" min="0.0" max="23.75" step="0.25" required="required"
											id="task${taskValue.id}-enteredHours-friday" value="0.0"
											class="form-control input-sm allowNumericWithDecimal" /></td>
											<td><input type="text" readonly="readonly"
											id="task${taskValue.id}-subTotal" value="0.0"
											class="form-control input-sm allowNumericWithDecimal" /></td>
									</tr>
									<input type="hidden" id="task${taskValue.id}-id"
										value="<c:out value="${userSubmittedTimeLogsList.userSubmittedTimeLogsList[counter].id}"/>"
										name="userSubmittedTimeLogsList[${counter}].id" />
									<c:if test="${isAdminViewingTimesheet == true}">
										<input type="hidden" id="task${taskValue.id}-userId"
											value="<c:out value="${userTimesheetBelongsTo}"/>"
											name="userSubmittedTimeLogsList[${counter}].user.id" />
									</c:if>
									<c:if test="${isAdminViewingTimesheet != true}">
										<input type="hidden" id="task${taskValue.id}-userId"
											value="<c:out value="${loggedInUser}"/>"
											name="userSubmittedTimeLogsList[${counter}].user.id" />
									</c:if>
									<input type="hidden" id="task${taskValue.id}-taskId"
										value="${taskValue.id}"
										name="userSubmittedTimeLogsList[${counter}].task.id" />
									<input type="hidden"
										id="task${taskValue.id}-hoursPerDayPerWeek"
										value="<c:out value="${userSubmittedTimeLogsList.userSubmittedTimeLogsList[counter].hoursPerDayPerWeek}"/>"
										name="userSubmittedTimeLogsList[${counter}].hoursPerDayPerWeek" />
									<input type="hidden" id="task${taskValue.id}-totalHoursPerWeek"
										value="<c:out value="${userSubmittedTimeLogsList.userSubmittedTimeLogsList[counter].totalHoursPerWeek}"/>"
										name="userSubmittedTimeLogsList[${counter}].totalHoursPerWeek" />
									<input type="hidden" id="task${taskValue.id}-date"
										value="<c:out value="${userSubmittedTimeLogsList.userSubmittedTimeLogsList[counter].date}"/>"
										name="userSubmittedTimeLogsList[${counter}].date" />
									<c:if
										test="${not empty userSubmittedTimeLogsList.userSubmittedTimeLogsList}">
										<input type="hidden" id="task${taskValue.id}-status"
											value="Resubmitted"
											name="userSubmittedTimeLogsList[${counter}].status" />
									</c:if>
									<c:if
										test="${empty userSubmittedTimeLogsList.userSubmittedTimeLogsList}">
										<input type="hidden" id="task${taskValue.id}-status"
											value="Submitted"
											name="userSubmittedTimeLogsList[${counter}].status" />
									</c:if>
									<c:set var="counter" value="${counter + 1}" />
								</c:forEach>
							</c:forEach>
							<tr style="background-color:#222;">
								<td colspan="9"></td>
								
							</tr>
							<tr>
									<td title="Personal Time Off" style="padding-left:1%;  font-weight: bold;">PTO</td>
									<td><input id="ptoHours-saturday" value="0.0" type="number" min="0.0" max="23.75" step="0.25" required="required" class="form-control input-sm allowNumericWithDecimal" /></td>
									<td><input id="ptoHours-sunday" value="0.0" type="number" min="0.0" max="23.75" step="0.25" required="required" class="form-control input-sm allowNumericWithDecimal" /></td>
									<td><input id="ptoHours-monday" value="0.0" type="number" min="0.0" max="23.75" step="0.25" required="required" class="form-control input-sm allowNumericWithDecimal" /></td>
									<td><input id="ptoHours-tuesday" value="0.0" type="number" min="0.0" max="23.75" step="0.25" required="required" class="form-control input-sm allowNumericWithDecimal" /></td>
									<td><input id="ptoHours-wednesdayday" value="0.0" type="number" min="0.0" max="23.75" step="0.25" required="required" class="form-control input-sm allowNumericWithDecimal" /></td>
									<td><input id="ptoHours-thursday" value="0.0" type="number" min="0.0" max="23.75" step="0.25" required="required" class="form-control input-sm allowNumericWithDecimal" /></td>
									<td><input id="ptoHours-friday" value="0.0" type="number" min="0.0" max="23.75" step="0.25" required="required" class="form-control input-sm allowNumericWithDecimal" /></td>
									<td style="margin-top:2%;">
										<input id="totalPtoHours" readonly="readonly" value="0.0" type="number" min="0.0" max="23.75" step="0.25" required="required" class="form-control input-sm allowNumericWithDecimal" />
										<input id="totalPtoHoursPerWeek" name="totalPtoHoursPerWeek" type="hidden" value="${totalPtoHoursPerWeek}" />
									</td>
								</tr>
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td colspan="2" style="padding-left:7%;  font-weight: bold;">Total Work Hours</td>
								<td style="font-weight: bold;"><input type="text" readonly="readonly" id="grandTotal" value="0.0" class="form-control input-sm allowNumericWithDecimal grdtot" /></td>
							</tr>
						</tfoot>
					</table>
				</c:when>
			</c:choose>
			<c:if test="${(error == null) and (success == null) and (empty tasksForCurrentUserPerModuleMap)}">
				<div class="alert alert-danger">
					<p>You were not allocated on any task during this week.</p>
				</div>
			</c:if>
		</form:form>
		
		<c:if test="${isAdminViewingTimesheet == true}">
			<div class=" col-md-5 col-md-offset-8" style=" position: relative; left: 3%;">
				<input type="submit" id="submitButton" value="Update" class="btn btn-sm btn-primary" style="background-color: #626e7e;"/>
				<input type="submit" id="requestToResubmitTimesheetButton" value="Request to Resubmit" class="btn btn-sm" style="background-color: #d1d5db; border-color: #626e7e"/>
				<input type="submit" id="approveTimesheetButton" value="Approve" class="btn btn-primary btn-sm" style="background-color: #626e7e;"/>
				<a href="${pageContext.request.contextPath}/team-timesheets"  class="btn btn-sm btn-primary">Cancel</a>
			</div>
			
		</c:if>
		<c:if test="${(error == null) and (success == null) and (not empty tasksForCurrentUserPerModuleMap) and (isAdminViewingTimesheet != true)}">
			<div class="col-md-2 col-md-offset-10" style=" position: relative; left: 5%;" id="submitButtonContainer">
				<input type="submit" id="submitButton" value="Submit" class="btn btn-sm btn-primary" style="background-color: #626e7e;"/>
				<a href="${pageContext.request.contextPath}/view-timesheets"  class="btn btn-sm btn-primary">Cancel</a>
			</div>
		</c:if>
		<c:if test="${isAdminViewingTimesheet == false}">
			<a href="${pageContext.request.contextPath}/view-timesheets" style="float:right;">Redirect to timesheets list</a>
		</c:if>
	</div>
</body>