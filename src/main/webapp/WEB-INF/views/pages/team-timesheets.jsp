<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<title>Instep Time App - Timesheets</title>
<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/timesheets-list-grid.css' />" rel="stylesheet"></link>
<!-- <link rel="stylesheet" type="text/css"
	href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" /> -->
	<script src="<c:url value='/static/js/jquery.dataTables.min.js' />"></script>
<link href="<c:url value='/static/css/jquery.dataTables.min.css' />" rel="stylesheet"></link> 
<script type="text/javascript">
$(document).ready(function() {
	$('#teamTimesheetsTable').DataTable();
});
</script>	
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="form-group col-md-12">
				<h3 class="col-md-5 control-label">Team Timesheets </h3>
			</div>
		</div>
			<c:choose>
				<c:when test="${not empty userSubmittedTimeLogsListByStatusAndUserRole}">
					<div id="teamTimesheetsTableWrapper" style="padding: 2%;">
						<table id="teamTimesheetsTable" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Employee Name </th>
									<th>Week End Date</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${userSubmittedTimeLogsListByStatusAndUserRole}" var="userSubmittedTimeLog" varStatus="loop">
									<tr>
										<td>${userSubmittedTimeLog.user.firstName} ${userSubmittedTimeLog.user.lastName}</td>
										<td>${userSubmittedTimeLog.date}</td>
										<td>${userSubmittedTimeLog.status}</td>
										<td><a href="${pageContext.request.contextPath}/time-approval-${userSubmittedTimeLog.user.id}_${userSubmittedTimeLog.date}" class="btn btn-default btn-sm btn-block"> View </a></td>
									</tr>
								</c:forEach>									
							</tbody>
						</table>
					</div>		
			</c:when>
			<c:otherwise>
				<div class="alert alert-warning">
					<p>Currently, there aren't any timesheets to approve.</p>
				</div>
			</c:otherwise>
		</c:choose>
		
		
		<%-- <c:choose>
			<c:when test="${not empty userSubmittedTimeLogsListByStatusAndUserRole}">
				<div class="row">
					<div class="form-group col-md-3 col-header">
						<label class="col-md-8 control-label" for="" style="float:none"> Employee </label>
					</div>
					<div class="form-group col-md-4 col-header">
						<label class="col-md-8 control-label" for="" style="float:none"> Log Date </label>
					</div>
					<div class="form-group col-md-3 col-header">
						<label class="col-md-8 control-label" for="" style="float:none"> Status </label>
					</div>
					<div class="col-sm-2"></div>
				</div>
				<c:forEach items="${userSubmittedTimeLogsListByStatusAndUserRole}"
					var="userSubmittedTimeLog" varStatus="loop">
					<div class="row">
						<div class="form-group col-md-3">
							<label class="col-md-8 control-label" for="" style="float:right">
								${userSubmittedTimeLog.user.firstName} </label>
						</div>
						<div class="form-group col-md-4">
							<label class="col-md-8 control-label" for="" style="float:right">
								${userSubmittedTimeLog.date} </label>
						</div>
						<div class="form-group col-md-3">
							<label class="col-md-8 control-label" for="" style="float:right">
								${userSubmittedTimeLog.status} </label>
						</div>
						<div class="form-group col-sm-2">
							<a href="${pageContext.request.contextPath}/time-approval-${userSubmittedTimeLog.user.id}_${userSubmittedTimeLog.date}" class="btn btn-default btn-sm btn-block">
								View </a>
						</div>
					</div>
					<hr style="margin: unset; border-top: 1px solid #626e7e;">
				</c:forEach>
			</c:when>
			<c:otherwise>
				<div class="alert alert-warning">
					<p>Currently, there aren't any timesheets to approve.</p>
				</div>
			</c:otherwise>
		</c:choose> --%>
	</div>
</body>