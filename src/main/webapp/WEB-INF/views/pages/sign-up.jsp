<%@ page language="java" contentType="text/html; charset=ISO-8859-15" pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
		<title>Instep Time App - Sign Up</title>
		<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
		<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
		<script src="<c:url value='/static/js/sign-up.js' />"></script>
		<script src="<c:url value='/static/js/password-validation.js' />"></script>
		<link href="<c:url value='/static/css/bootstrap.css' />"  rel="stylesheet"></link>
		<link href="<c:url value='/static/css/login.css' />"  rel="stylesheet"></link>
		<link href="<c:url value='/static/css/password-validation.css' />"  rel="stylesheet"></link>
		<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
		<!-- <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" /> -->
	</head>

<body>
	<div class="container">
	    <div class="row">
	        <div class="col-sm-6 col-md-4 col-md-offset-4">
	        <h1 class="text-center login-title" style="font-size:2em;">Instep Time App</h1>
	            <h1 class="text-center login-title">Sign Up</h1>
	            <div class="account-wall">
	            <div class="form-signin">
	                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
	                    alt="">
	                <c:url var="signUpUrl" value="/sign-up" />
	                <!-- <form class="form-signin" method="post" class="form-horizontal"> -->
	                
						<div style="display:none" id="userRegistrationSuccessfulLabel" class="alert alert-success">
							<p>Password created. You can login now.</p>
						</div>
	                	<div style="display:none" id="usernameNotRegisteredLabel" class="alert alert-danger">
							<p>Username not registered</p>
						</div>
					<div class="input-group input-lg">
						<label class="input-group-addon" for="username"><i class="fa fa-user"></i></label>
						<input type="text" class="form-control" readonly="readonly" id="username" name="username" placeholder="Enter provided username" required="required" value="${username}.com">
					</div>
					<div class="input-group input-lg">
						<label class="input-group-addon" for="password"><i class="fa fa-lock"></i></label> 
	                	<input type="password" class="form-control" id="password" name="password" placeholder="Enter password"  required="required" autofocus="autofocus">
	                </div>
	                <div class="input-group input-lg">
						<label class="input-group-addon" for="password"><i class="fa fa-lock"></i></label> 
	                	<input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm password"  required="required">
	                </div>
	                <div class="input-group input-sm" style="margin:0px 0px 4px 1px ;">
	                	<span id="password-validation-result" style="padding-top: 8px; padding-bottom: 8px; margin:0px 0px 4px 6px ;"></span>
	                </div>
	                 <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
	                <!-- <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span> -->
	                <!-- </form> -->
	                <a class="btn btn-md btn-info btn-block" id="signUpButton" disabled>Sign Up</a>
	                <a href="${pageContext.request.contextPath}/login" class="btn btn-md btn-primary btn-block">Sign In</a>
	                </div>
	            </div>
	            <!-- <a href="#" class="text-center new-account">Create an account </a> -->
	        </div>
	    </div>
	</div>
</body>
</html>