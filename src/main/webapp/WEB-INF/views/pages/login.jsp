<%@ page language="java" contentType="text/html; charset=ISO-8859-15" pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
		<title>Instep Time App - Login</title>
		<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
		<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
		<script src="<c:url value='/static/js/login.js' />"></script>
		<link href="<c:url value='/static/css/bootstrap.css' />"  rel="stylesheet"></link>
		<link href="<c:url value='/static/css/login.css' />"  rel="stylesheet"></link>
		<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
	</head>

<body>
	<div class="container">
		<div class="row">
	        <div class="col-sm-6 col-md-4 col-md-offset-4">
	            <h1 class="text-center login-title">Log In</h1>
	            <div class="account-wall">
	                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
	                    alt="">
	                <c:url var="loginUrl" value="/login" />
	                <form class="form-signin"  action="${loginUrl}" method="post" class="form-horizontal">
						<c:if test="${param.error != null}">
							<div class="alert alert-danger">
								<p>Invalid username or password</p>
							</div>
						</c:if>
						<c:if test="${param.logout != null}">
							<div class="alert alert-success">
								<p>You are successfully logged out.</p>
							</div>
						</c:if>
	                <div class="input-group input-lg">
						<label class="input-group-addon" for="username"><i class="fa fa-user"></i></label>
						<input type="text" class="form-control" id="username" name="username" placeholder="Enter username" required autofocus>
					</div>
					<div class="input-group input-lg">
						<label class="input-group-addon" for="password"><i class="fa fa-lock"></i></label> 
	                	<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
	                </div>
	                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Sign In" />
	                <label class="checkbox pull-left">
	                    <input type="checkbox" id="rememberme" name="remember-me">
	                    remember me
	                </label>
	                <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
	                <!-- <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span> -->
	                </form>
	            </div>
	            <!-- <a href="#" class="text-center new-account">Create an account </a> -->
	        </div>
	    </div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	    <%-- <div class="row">
	        <div class="col-sm-6 col-md-4 col-md-offset-4">
	        	<h1 class="text-center login-title" style="font-size:2em;">Instep Time App</h1>
	            <h1 class="text-center login-title">Login</h1>
	            <div class="account-wall">
	                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
	                    alt="">
	                <c:url var="loginUrl" value="/login" />
	                <form class="form-signin" action="${loginUrl}" method="post" class="form-horizontal">
						<c:if test="${param.error != null}">
							<div class="alert alert-danger">
							Current Locale : ${pageContext.response.locale}
								<p>Invalid username or password</p>
							</div>
						</c:if>
						<c:if test="${param.logout != null}">
							<div class="alert alert-success">
								<p>You are successfully logged out.</p>
							</div>
						</c:if>
	                <div class="input-group input-lg">
						<label class="input-group-addon" for="username"><i class="fa fa-user"></i></label>
						<input type="text" class="form-control" id="username" name="username" placeholder="Enter username" required autofocus>
					</div>
					<div class="input-group input-lg">
						<label class="input-group-addon" for="password"><i class="fa fa-lock"></i></label> 
	                	<input type="password" class="form-control" id="password" name="password" placeholder="Enter password" required>
	                </div>
	               <label  class="btn btn-md btn-primary btn-block" id="signInButton" >Sign In</label>
	                 <a href="${pageContext.request.contextPath}/sign-up" class="btn btn-md btn-info btn-block">Sign Up</a>
	                <!-- <label class="checkbox pull-left">
	                    <input type="checkbox" id="rememberme" name="remember-me">
	                    Remember Me
	                </label> -->
	                <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
	                <!-- <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span> -->
	                </form>
	            </div>
	            <!-- <a href="#" class="text-center new-account">Create an account </a> -->
	        </div>
	    </div> --%>
	</div>
</body>
</html>