<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<title>Instep Time App - View Timesheets</title>
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link> 
<link href="<c:url value='/static/css/timesheets-list-grid.css' />" rel="stylesheet"></link>
<script src="<c:url value='/static/js/jquery.dataTables.min.js' />"></script>
<link href="<c:url value='/static/css/jquery.dataTables.min.css' />" rel="stylesheet"></link> 
<script type="text/javascript">
$(document).ready(function() {
	$('#teamTimesheetsTable').DataTable();
});
</script>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="form-group col-md-12">
				<h3 class="col-md-5 control-label">Timesheets list</h3>
			</div>
		</div>
		
		<table id="viewTimesheetsTable" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Start Date </th>
					<th>End Date</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:set var="prevFiveWeeksCounter" value="4" />
				<c:forEach items="${previousFiveWeeksMap}" var="entry" varStatus="loop">
				<tr>
					<td>${entry.value[0]}</td>
					<td>${entry.value[6]}</td>
					<td>${previousFiveWeeksStatusMap['week-' += prevFiveWeeksCounter]}</td>
					<td>
						<c:choose>
						<c:when test="${previousFiveWeeksStatusMap['week-' += prevFiveWeeksCounter] == 'Open' || previousFiveWeeksStatusMap['week-' += prevFiveWeeksCounter] == 'Reopened'}">
							<a href="${pageContext.request.contextPath}/time-entry-${entry.value}" > Edit </a>
						</c:when>
						<c:otherwise>
							<a href="${pageContext.request.contextPath}/time-entry-${entry.value}"> View </a>
						</c:otherwise>
					</c:choose>
					</td>
				</tr>
				<c:set var="prevFiveWeeksCounter" value="${prevFiveWeeksCounter - 1}" />
				</c:forEach>
				
				<c:set var="currentWeekAndUpcomingFourWeeksCounter" value="0" />
				<c:forEach items="${currentWeekAndUpcomingFourWeeksMap}" var="entry" varStatus="loop">
					<tr>
						<td>${currentWeekAndUpcomingFourWeeksMap['week-' += currentWeekAndUpcomingFourWeeksCounter][0]}</td>
						<td>${currentWeekAndUpcomingFourWeeksMap['week-' += currentWeekAndUpcomingFourWeeksCounter][6]}</td>
						<td>${currentWeekAndUpcomingFourWeeksStatusMap['week-' += currentWeekAndUpcomingFourWeeksCounter]}</td>
						<td>
							<c:choose>
								<c:when test="${currentWeekAndUpcomingFourWeeksStatusMap['week-' += currentWeekAndUpcomingFourWeeksCounter] == 'Open' || currentWeekAndUpcomingFourWeeksStatusMap['week-' += currentWeekAndUpcomingFourWeeksCounter] == 'Reopened'}">
									<a href="${pageContext.request.contextPath}/time-entry-${currentWeekAndUpcomingFourWeeksMap['week-' += currentWeekAndUpcomingFourWeeksCounter]}" > Edit </a>
								</c:when>
								<c:otherwise>
									<a href="${pageContext.request.contextPath}/time-entry-${currentWeekAndUpcomingFourWeeksMap['week-' += currentWeekAndUpcomingFourWeeksCounter]}" > View </a>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<c:set var="currentWeekAndUpcomingFourWeeksCounter" value="${currentWeekAndUpcomingFourWeeksCounter + 1}" />
				</c:forEach>
				
			</tbody>
		</table>
	</div>
</body>