<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<c:choose>
	<c:when test="${edit}">
		<title>Instep Time App - Edit Task</title>
	</c:when>
	<c:otherwise>
		<title>Instep Time App - Add Task</title>
	</c:otherwise>
</c:choose>
<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<script src="<c:url value='/static/js/admin-management.js' />"></script>
<link href="<c:url value='/static/css/bootstrap.css' />"
	rel="stylesheet"></link>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
<!-- <link rel="stylesheet" type="text/css"
	href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" /> -->
<script>
var moduleVal = '<c:out value="${task.module.id}"/>';
$(document).ready(function() {
	toggleSubmitButtonDisable();
	getModulesForSelectedProject();
	$("#projectsListDropDown").change(getModulesForSelectedProject);
	$("#modulesListDropDown").change(getTasksForSelectedModule);
});
function getModulesForSelectedProject() {
	toggleSubmitButtonDisable();
	var moduleDropdownDefaultSelectOption = '<option value="-1">Choose Module</option>';
	var noModulesAllocatedToProject = '<option value="-1">No modules allocated to the project yet.</option>';
	if($("#projectsListDropDown").val() == -1){
		$("#modulesListDropDown").empty().append("<option value=\"-1\">Choose Project First</option>");
		$("#tasksListDropDown").empty().append("<option value=\"-1\">Choose Module First</option>");
	} else {
		$.getJSON("../InstepTimeManagementApp/getModulesForSelectedProject",{projectId: $("#projectsListDropDown").val()}, function(j){
			console.log(j);
			var options = '';
			options += moduleDropdownDefaultSelectOption;
			for (var i = 0; i < j.length; i++) {
				options += '<option value="' + j[i].id + '">' + j[i].name + '</option>';
			}
			$("#modulesListDropDown").html(options);
			debugger;
			$("#modulesListDropDown  option").each( function() {
				if (this.value == moduleVal) {
					$(this).attr("selected", "selected");
				}
			});
		}).error(function() { $("#modulesListDropDown").html(noModulesAllocatedToProject);  });
	}
}
</script>
</head>

<body>
	<div class="container">
		<form:form id="addTaskForm" method="POST" modelAttribute="task" class="form-horizontal">
		<div class="row">
			<c:choose>
				<c:when test="${edit}">
					<div class="form-group col-md-12">
				<h3 class="col-md-3 ">Update Task </h3>
			</div>
				</c:when>
				<c:otherwise>
					<div class="form-group col-md-12">
				<h3 class="col-md-3 ">Add Task </h3>
			</div>
				</c:otherwise>
			</c:choose>
		</div>
			<div class="row">
				<c:if test="${error != null}">
					<div class="alert alert-danger">
						<p>${error}</p>
					</div>
				</c:if>
				<c:if test="${success != null}">
					<div class="alert alert-success">
						<p>${success}</p>
					</div>
				</c:if>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="name">Task Name </label>
					<div class="col-md-3">
						<form:input type="text" path="name" id="name"
							class="form-control input-sm" required="required" />
						<div class="has-error">
							<form:errors path="name" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="description">Description </label>
					<div class="col-md-3">
						<form:input type="text" path="description" id="description"
							class="form-control input-sm" required="required" />
						<div class="has-error">
							<form:errors path="description" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<c:choose>
			<c:when test="${edit}">
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="module.project">Project
					</label>
					<div class="col-md-3">
						<select name="project" id="projectsListDropDown" class="form-control input-sm">
							<option value="-1">Choose Project</option>
							<c:forEach items="${projectsList}" var="project">
								<option class="form-control input-sm" value="${project.id}"
									${project.id == task.module.project.id  ? 'selected' : ''}>
									${project.name}</option>
							</c:forEach>
						</select>
						<div class="has-error">
							<form:errors path="module.project" class="help-inline" />
						</div>
					</div>
					<!-- <div class="col-md-3">
						<label class="btn btn-xs" id="getModulesForSelectedProjectButton" style="width: 70%; background-color: #d1d5db;"> Fetch Modules </label>
					</div> -->
				</div>
			</div>
			<input type="hidden" value="${module.project.id}" id="hiddenProjectIdField"/> 
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="module">Module
					</label>
					<div class="col-md-3">
						<select name="module" id="modulesListDropDown" class="form-control input-sm">
						</select>
						<div class="has-error">
							<form:errors path="module" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" value="${task.module.id}" id="hiddenModuleIdField"/>
			</c:when>
			<c:otherwise>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="module.project">Project
					</label>
					<div class="col-md-3">
						<form:select path="module.project" id="projectsListDropDown" class="form-control input-sm">
								<form:option value="-1">Choose Project</form:option>
								<form:options itemLabel="name" itemValue="id" items="${projectsList}" />
						</form:select>
						<div class="has-error">
							<form:errors path="module.project" class="help-inline" />
						</div>
					</div>
					<!-- <div class="col-md-3">
						<label class="btn btn-xs" id="getModulesForSelectedProjectButton" style="width: 70%; background-color: #d1d5db;"> Fetch Modules </label>
					</div> -->
				</div>
			</div> 
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="module">Module
					</label>
					<div class="col-md-3">
						<select name="module" id="modulesListDropDown" class="form-control input-sm">
						</select>
						<div class="has-error">
							<form:errors path="module" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			</c:otherwise>
			</c:choose>
			
			<c:choose>
				<c:when test="${edit}">
					<div class="form-group col-md-12">
						<div class="col-md-2  col-md-offset-3" style="position:relative; left:3.5%;">
						<input id="submitButton"  type="submit" value="Update" class="btn btn-sm btn-primary"/>
						<a href="${pageContext.request.contextPath}/view-tasks"  class="btn btn-sm btn-primary">Cancel</a>
					</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="form-group col-md-12">
						<div class="col-md-2  col-md-offset-3" style="position:relative; left:3.5%;">
							<input id="submitButton"  type="submit" value="Create" class="btn btn-sm btn-primary"  />
							<a href="${pageContext.request.contextPath}/view-tasks"  class="btn btn-sm btn-primary">Cancel</a>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			</form:form>
	</div>
</body>
</html>