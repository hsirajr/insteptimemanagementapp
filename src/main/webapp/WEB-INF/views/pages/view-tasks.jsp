<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<title>Instep Time App - View Tasks</title>
<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link> 
<link href="<c:url value='/static/css/timesheets-list-grid.css' />" rel="stylesheet"></link>
<!-- <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" /> -->
<script src="<c:url value='/static/js/jquery.dataTables.min.js' />"></script>
<link href="<c:url value='/static/css/jquery.dataTables.min.css' />" rel="stylesheet"></link> 
<script type="text/javascript">
$(document).ready(function() {
	$('#tasksTable').DataTable();
});
</script>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="form-group col-md-12">
				<h3 class="col-md-5 control-label">View All Tasks </h3>
			</div>
		</div>
			<div class="row">
				<div class="col-md-12" >
					<c:if test="${error != null}">
						<div class="alert alert-danger">
							${error}
						</div>
					</c:if>
					<c:if test="${success != null}">
						<div class="alert alert-success">
							${success}
						</div>
					</c:if>
				</div>
			</div>
		<div id="tasksTableWrapper" style="padding: 2%;">
				<table id="tasksTable"
					class="table table-striped dt-responsive nowrap"
					cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Task Name</th>
							<th>Project</th>
							<th>Module</th>
							<th>Status</th>
							<th width="100"></th>
							<th width="100"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${tasks}" var="task">
							<tr>
								<td>${task.name}</td>
								<td>${task.module.project.name}</td>
								<td>${task.module.name}</td>
								<td>${task.status}</td>
								<td><a href="<c:url value='/edit-task-${task.id}' />"
										class="btn btn-sm" style="color: #f5f5f5; background-color: #2B2B2B;">Edit</a></td>
								<td>
									<c:choose>
										<c:when test="${task.status == 'Active' }">
											<a href="<c:url value='/inactivate-task-${task.id}' />" class="btn btn-sm btn-default" onclick="return confirm('Are you sure you want to inactivate this task \?');">Inactivate</a>
										</c:when>
										<c:otherwise>
											<a href="<c:url value='/inactivate-task-${task.id}' />" class="btn btn-sm btn-default" onclick="return confirm('Are you sure you want to activate this task ?');" >Active</a>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
	</div>
</body>