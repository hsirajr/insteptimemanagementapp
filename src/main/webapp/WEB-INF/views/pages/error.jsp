<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<title>Error</title>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
</head>
<body>
	<div class="generic-container">
		<div>
			<h1 style="text-align: center; line-height: 10em;"> Oops something went wrong ! <a href="${pageContext.request.contextPath}/logout" class="btn btn-danger custom-width" >Logout</a>
			</h1>
		</div>
	</div>
</body>
</html>
