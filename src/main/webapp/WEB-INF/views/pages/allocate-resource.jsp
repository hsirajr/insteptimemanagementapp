<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<title>Instep Time App - Allocate Resource</title>
<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<!-- <script type="text/javascript">var moduleDropdownDefaultSelectOption = '<option value="-1">Choose Module</option>';</script>
<script type="text/javascript">var taskDropdownDefaultSelectOption = '<option value="-1">Choose Task</option>';</script>-->
 <script src="<c:url value='/static/js/admin-management.js' />"></script>
<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
<!-- <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" /> -->
<script type="text/javascript">
$(document).ready(function() {
	
});
</script>
	
</head>
<body>
	<div class="container">
	<div class="row">
			<div class="form-group col-md-12">
				<h3 class="col-md-5 ">Allocate Resources</h3>
			</div>
		</div>
	<form:form id="allocateResourcesForm" method="POST" modelAttribute="task"
			class="form-horizontal">
			<div style="display: none" id="resourceAllocationFailedLabel" class="alert alert-danger">
				<p>An error occurred while allocating the resources.</p>
			</div>
			<div style="display: none" id="resourceAllocationSuccessfulLabel" class="alert alert-success">
				<p>Resources successfully allocated.</p>
			</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-2 " for="module.project">Project
				</label>
				<div class="col-md-3">
					<form:select path="module.project" id="projectsListDropDown" style="width: 100%" class="multiselect-ui form-control" >
							<option value="-1">Choose Project</option>
							<form:options itemLabel="name" itemValue="id" items="${projectsList}" />
					</form:select>
					<div class="has-error">
						<form:errors path="module.project" class="help-inline" />
					</div>
				</div>
			</div>
		</div> 
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-2 " for="module">Module
				</label>
				<div class="col-md-3">
					<select name="module" id="modulesListDropDown" style="width: 100%" class="multiselect-ui form-control" >
					</select>
					<div class="has-error">
						<form:errors path="module" class="help-inline" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-2 " for="id">Task
				</label>
				<div class="col-md-3">
					<select name="id" id="tasksListDropDown" style="width: 100%" class="multiselect-ui form-control" >
					</select>
					<div class="has-error">
						<form:errors path="id" class="help-inline" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<label class="col-md-2 " for="users">Non Allocated Resources
				</label>
				<div class="col-md-3">
					<select name="users" id="nonAllocatedUsersListDropDown" class="multiselect-ui form-control" multiple="multiple" style="min-height:15%;width: 100%">
					</select>
					<div class="has-error">
						<form:errors path="users" class="help-inline" />
					</div>
				</div>
				
				
				<div class="col-md-2">
				<input title="Allocate" class="btn btn-sm" style="margin:1%; color: #f5f5f5; background-color: #2B2B2B;" type="button" id="right" value=">"/> <br />
				<input title="Allocate All" class="btn btn-sm" style="margin:1%; color: #f5f5f5; background-color: #2B2B2B;" type="button" id="rightall" value=">>"/><br />
				<input title="Deallocate" class="btn btn-sm" style="margin:1%; color: #f5f5f5; background-color: #2B2B2B;" type="button" id="left" value="<"/> <br />
				<input title="Deallocate All" class="btn btn-sm" style="margin:1%; color: #f5f5f5; background-color: #2B2B2B;" type="button" id="leftall" value="<<"/> 
				</div>
				
				
				<label class="col-md-2 ">Allocated Resources
				</label>
				<div class="col-md-3">
					<select name="users" id="allocatedUsersListDropDown" class="multiselect-ui form-control" multiple="multiple" style="min-height:15%;width: 100%">
					</select>
					<div class="has-error">
						<form:errors path="users" class="help-inline" />
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="taskNameHiddenField" name="name" />
		</form:form> 
		<div class="col-md-12 col-md-offset-10">
			<input type="submit" id="allocateResourcesFormSubmitButton" disabled="disabled" value="Update Allocation" class="btn btn-sm" style="color: #f5f5f5; background-color: #2B2B2B; position:relative; left:2.8%;"/>
		</div>
	</div>
</body>
</html>