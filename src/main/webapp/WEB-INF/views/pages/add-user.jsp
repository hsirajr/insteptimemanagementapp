<%@ page language="java" contentType="text/html; charset=ISO-8859-15"
	pageEncoding="ISO-8859-15"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
<c:choose>
	<c:when test="${edit}">
		<title>Instep Time App - Edit User</title>
	</c:when>
	<c:otherwise>
		<title>Instep Time App - Add User</title>
	</c:otherwise>
</c:choose>
<script src="<c:url value='/static/js/jquery-1.11.1.min.js' />"></script>
<script src="<c:url value='/static/js/admin-management.js' />"></script>
<link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"></link>
<link href="<c:url value='/static/css/font-awesome.css' />" rel="stylesheet"></link>
<!-- <link rel="stylesheet" type="text/css"
	href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css" /> -->
</head>
<body>
	<div class="container">
		<form:form id="addUserForm" method="POST" modelAttribute="user" class="form-horizontal">
		<input id="userId" type="hidden" value="${user.id}" />
		<div style="display: none" id="resetPasswordFailedLabel" class="alert alert-danger">
				<p>Unable to send reset password request.</p>
			</div>
			<div style="display: none" id="resetPasswordSuccessfulLabel" class="alert alert-success">
				<p>Password reset request sent successfully</p>
			</div>
		<div class="row">
			<c:choose>
				<c:when test="${edit}">
					<div class="form-group col-md-12">
				<h3 class="col-md-3 ">Update User </h3>
			</div>
				</c:when>
				<c:otherwise>
					<div class="form-group col-md-12">
				<h3 class="col-md-3 ">Add User </h3>
			</div>
				</c:otherwise>
			</c:choose>
		</div>
			<c:if test="${error != null}">
				<div class="alert alert-danger">
					<p>${error}</p>
				</div>
			</c:if>
			<c:if test="${success != null}">
				<div class="alert alert-success">
					<p>${success}</p>
				</div>
			</c:if>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="firstName">First
						Name </label>
					<div class="col-md-3">
						<form:input type="text" path="firstName" id="firstName"
							class="form-control input-sm" required="required" />
						<div class="has-error">
							<form:errors path="firstName" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="lastName">Last
						Name </label>
					<div class="col-md-3">
						<form:input type="text" path="lastName" id="lastName"
							class="form-control input-sm" required="required" />
						<div class="has-error">
							<form:errors path="lastName" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="username">Username
					</label>
					<div class="col-md-3">
						<c:choose>
							<c:when test="${edit}">
								<form:input readonly="true" type="text" path="username" id="username" class="form-control input-sm" />
							</c:when>
							<c:otherwise>
								<form:input type="text" path="username" id="username" class="form-control input-sm" required="required" /> <span>@instepinc.com</span>
							</c:otherwise>
						</c:choose>	
						<div class="has-error">
							<form:errors path="username" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			<%-- <c:if test="${edit}">
				<div class="row">
					<div class="form-group col-md-12">
						<label class="col-md-2 " for="password">Password
						</label>
						<div class="col-md-3">
							<form:input type="password" path="password" id="password"
								class="form-control input-sm" />
							<div class="has-error">
								<form:errors path="password" class="help-inline" />
							</div>
						</div>
					</div>
				</div>
			</c:if> --%>
			
			<c:choose>
				<c:when test="${edit}">
					<div class="row">
						<div class="form-group col-md-12">
							<label class="col-md-2 " for="leadId">Lead
								Name </label>
							<div class="col-md-3">
								<select id="leadsListDropDown" name="leadId" class="form-control input-sm">
									<option value="-1">Choose Lead</option>
									<c:forEach items="${usersListWithAdminRole}" var="lead">
										<option value="${lead.id}" ${lead.id == user.leadId  ? 'selected' : ''} >${lead.firstName} ${lead.lastName}</option>
									</c:forEach>
								</select>
								<%-- <form:select path="leadId" class="form-control input-sm">
									<form:options itemLabel="firstName" itemValue="id" items="${usersListWithAdminRole}" />
								</form:select> --%>
								<div class="has-error">
									<form:errors path="leadId" class="help-inline" />
								</div>
							</div>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="row">
						<div class="form-group col-md-12">
							<label class="col-md-2 " for="leadId">Lead
								Name </label>
							<div class="col-md-3">
								<select id="leadsListDropDown" name="leadId" class="form-control input-sm">
									<option value="-1">Choose Lead</option>
									<c:forEach items="${usersListWithAdminRole}" var="lead">
										<option value="${lead.id}">${lead.firstName} ${lead.lastName}</option>
									</c:forEach>
								</select>
								<%-- <form:select path="leadId" class="form-control input-sm">
									<form:options itemLabel="firstName" itemValue="id" items="${usersListWithAdminRole}" />
								</form:select> --%>
								<div class="has-error">
									<form:errors path="leadId" class="help-inline" />
								</div>
							</div>
						</div>
					</div>	
				</c:otherwise>
			</c:choose>
			
					
			
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 " for="isAdmin">Is
						Admin? </label>
					<div class="col-md-3">
						<form:select path="isAdmin" class="form-control input-sm">
							<form:option value="No" label="No" />
							<form:option value="Yes" label="Yes" />
						</form:select>
						<div class="has-error">
							<form:errors path="isAdmin" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="form-group col-md-12">
					<label class="col-md-2 control-lable" for="userProfiles">Roles</label>
					<div class="col-md-3">
						<form:select path="userProfiles" items="${roles}" multiple="true"
							itemValue="id" itemLabel="type" class="form-control input-sm" />
						<div class="has-error">
							<form:errors path="userProfiles" class="help-inline" />
						</div>
					</div>
				</div>
			</div>
			
			<c:choose>
				<c:when test="${edit}">
					<div class="form-group col-md-12">
						<div class="col-md-3  col-md-offset-2" style="position:relative; left:1%;">
							<input id="submitButton" type="submit" value="Update" class="btn btn-sm btn-primary"/>
							<a onclick="resetPassword()" id="resetPassword" type="submit"  class="btn btn-sm btn-primary" >Reset Password</a>
							<a href="${pageContext.request.contextPath}/view-users"  class="btn btn-sm btn-primary">Cancel</a>
						</div>
					</div>
				</c:when>
				<c:otherwise> 
					<div class="form-group col-md-12">
						<div class="col-md-2  col-md-offset-3" style="position:relative; left:3%;">
							<input id="submitButton" type="submit" value="Create" class="btn btn-sm btn-primary"  />
							<a href="${pageContext.request.contextPath}/view-users"  class="btn btn-sm btn-primary">Cancel</a>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
		</form:form>
	</div>
</body>
</html>